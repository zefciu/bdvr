import copy
import io
import unittest

import pyramid.i18n

from bdvr.core.config import Configuration
from bdvr.core.expression._abc import Expression


CONFIG = Configuration('test_config.yaml')

class TestConfig(unittest.TestCase):
    """Tests for configuration."""

    def test_parsed(self):
        """Some elements in configuration are parsed expressions."""
        self.assertTrue(isinstance(
            CONFIG['rtypes']['knight']['fields']['slug']['slug_from'],
            Expression,
        ))

    def test_len(self):
        """Configuration has len == 3"""
        self.assertEqual(len(CONFIG), 3)

    def test_iter(self):
        """Configuration can be iterated"""
        self.assertSetEqual(set(CONFIG), {'meta', 'siteconfig', 'rtypes'})
    
    def test_gettext(self):
        """Autotranslate changes the interpretation of strings."""
        with open('test_config.yaml') as f:
            data = f.read()
        data = data.replace('autotranslate: true', 'autotranslate: false')
        dummy_file = io.StringIO(data)
        config2 = Configuration(dummy_file)
        self.assertTrue(
            isinstance(CONFIG['siteconfig']['title'],
                pyramid.i18n.TranslationString))
        self.assertFalse(
            isinstance(config2['siteconfig']['title'],
                pyramid.i18n.TranslationString))
        self.assertTrue(
            isinstance(config2['siteconfig']['title'], str))
