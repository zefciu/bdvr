"""Tests for search feature."""

import unittest

from bdvr.core.config import Local
from bdvr.core.db import migrate_eav, Connection
from bdvr.core.module import load_module
from bdvr.core.session import Session

from .mem_connection import MemConnection
from ._utils import do_migration


class TestSearch(unittest.TestCase):
    """Tests for search feature."""

    def setUp(self):
        self.local = Local('test_local.ini')
        migrate_eav(self.local.db_conn)
        self.connection = Connection(self.local.db_conn)
        self.module = load_module('core', self.connection)
        do_migration(self.module)
        Session.prepare(self.connection, self.module)
        self.session = Session(self.connection, self.module)
        self.admin = self.session.login(rtype_name='core.user', slug='admin')
        User = self.session.get_rtype('core.user')
        Group = self.session.get_rtype('core.group')
        self.wise_guys = Group(name='wise-guys')
        self.user1 = User(name='Bedevere',
            email='bedevere@camelot.uk', groups=[self.wise_guys]
        )
        self.user2 = User(name='Galahad',
            email='galahad@camelot.uk', groups=[]
        )
        self.session.commit()

    def tearDown(self):
        self.connection.rollback()
        migrate_eav(self.local.db_conn, towards=0)

    def test_search_by_related(self):
        User = self.session.get_rtype('core.user')
        session = Session(self.connection, self.module)
        s = session.search(User, ".groups.name == 'wise-guys'")
        self.assertSetEqual(set(s._fetch_ids()), {self.user1.__id__})
