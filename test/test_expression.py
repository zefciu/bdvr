import unittest

from bdvr.core.parser import parse_expression


class DummyResource():
    """Just an object in memory."""

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

BEDEVERE = DummyResource(
        education = 'knight', occupation='knight', name='Bedevere',
        nickname='The Wise')

class TestExpression(unittest.TestCase):
    """Tests for expression evaluation."""

    def testEq(self):
        """Equality operator."""
        compare_occ_edu = parse_expression('.education == .occupation')
        compare_name_nickname = parse_expression('.name == .nickname')
        self.assertTrue(compare_occ_edu.evaluate(BEDEVERE))
        self.assertFalse(compare_name_nickname.evaluate(BEDEVERE))

    def testNeq(self):
        """Equality operator."""
        compare_occ_edu = parse_expression('.education != .occupation')
        compare_name_nickname = parse_expression('.name != .nickname')
        self.assertFalse(compare_occ_edu.evaluate(BEDEVERE))
        self.assertTrue(compare_name_nickname.evaluate(BEDEVERE))

    def testBools(self):
        """Boolean logic operators."""
        occ_eq_edu_and_name_bedevere = parse_expression(
                ".education == .occupation & .name == 'Bedevere'")
        occ_eq_edu_or_name_galahad = parse_expression(
                ".education == .occupation | .name == 'Galahad'")
        occ_eq_edu_and_name_galahad = parse_expression(
                ".education == .occupation & .name == 'Galahad'")
        self.assertTrue(occ_eq_edu_and_name_bedevere.evaluate(BEDEVERE))
        self.assertTrue(occ_eq_edu_or_name_galahad.evaluate(BEDEVERE))
        self.assertFalse(occ_eq_edu_and_name_galahad.evaluate(BEDEVERE))
        self.assertSetEqual(
            occ_eq_edu_and_name_bedevere.get_paths(),
            {
                parse_expression('.education'),
                parse_expression('.occupation'),
                parse_expression('.name'),
            }
        )
        self.assertEqual(
            str(occ_eq_edu_and_name_bedevere),
            "((.education) == (.occupation)) & ((.name) == ('Bedevere'))",
        )
        self.assertEqual(
            occ_eq_edu_and_name_bedevere.get_js('null'),
            '''bdvr.getPath(null, ['education']) === \
bdvr.getPath(null, ['occupation']) && \
bdvr.getPath(null, ['name']) === \
"Bedevere"'''
        )
