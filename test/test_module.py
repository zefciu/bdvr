import imp
import os.path
import unittest

from bdvr.core.module import load_module, BdvrModule
from bdvr.core.field import Field
from bdvr.core.resource import Rtype

from .mem_connection import MemConnection

HERE = os.path.dirname(os.path.abspath(__file__))


class TestModule(unittest.TestCase):
    """Tests for the BDVR module class."""

    def setUp(self):
        self.connection = MemConnection()
        self.module = load_module('core', self.connection)

    def _migrate(self):
        """Make the migration and apply it."""
        version, code = self.module.make_migration()
        migration_scope = {}
        exec('\n'.join(code), migration_scope)
        migration_scope['forward'](self.connection)

    def test_migration(self):
        """Test a module migration from 0."""
        version, code = self.module.make_migration()
        self.assertIn(
            "    FTYPES['core.String'] = connection.add_ftype('core.String')",
            code
        )
        self.assertIn(
            "    RTYPES['core.user'] = connection.add_rtype('core.user', [])",
            code
        )
        self.assertIn(
            "    connection.add_field(RTYPES['core.user'], "
                "FTYPES['core.String'], 'name')",
            code
        )

    def test_further_migration(self):
        """Test a second migration."""
        self._migrate()
        module2 = BdvrModule(
            os.path.join(HERE, 'test_core_migration.yaml'),
            self.connection
        )
        version, code = module2.make_migration()
        self.assertIn(
            "    FTYPES['core.String'] = "
                "connection.get_ftype_id('core.String')",
            code
        )
        self.assertIn(
            "    RTYPES['core.user'] = connection.get_rtype_id('core.user')",
            code
        )
        self.assertNotIn(
            "    connection.add_field(RTYPES['core.user'], "
                "FTYPES['core.String'], 'name')",
            code
        )

    def test_sync(self):
        """Sync puts some synced ftypes and rtypes in module."""
        self._migrate()
        self.module.sync()
        self.assertTrue(issubclass(
            self.module.ftypes['core.Integer'],
            Field,
        ))
        self.assertTrue(isinstance(
            self.module.rtypes['core.user'],
            Rtype,
        ))

    def test_invalid_module(self):
        """The module that doesn't exist is handled by ValueError."""
        with self.assertRaises(ValueError):
            load_module('thisisnotamodulename', self.connection)
