import unittest

from bdvr.core.expression.comparison import Eq
from bdvr.core.expression.path import Path
from bdvr.core.parser import parser

class TestParser(unittest.TestCase):
    """Tests for expression parser."""

    def test_path(self):
        """Parsing path expressions."""
        path_a = parser.parse('.first.second.third')
        self.assertEqual(path_a, Path(['first', 'second', 'third']))
        self.assertEqual(str(path_a), '.first.second.third')

    def test_eq(self):
        """Parsing equality expression."""
        expr = parser.parse('.some.path == .other.path')
        self.assertEqual(
            expr, Eq(Path(['some', 'path']), Path(['other', 'path'])))
