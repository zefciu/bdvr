"""Tests for high level resource operations."""

import unittest

from bdvr.core.module import load_module
from bdvr.core.resource import dehydrate
from bdvr.core.session import Session
from .mem_connection import MemConnection
from ._utils import do_migration

class TestResource(unittest.TestCase):
    """Test a sample resource."""

    def setUp(self):
        self.connection = MemConnection()
        self.module = load_module('core', self.connection)
        do_migration(self.module)
        Session.prepare(self.connection, self.module)
        self.session = Session(self.connection, self.module)
        self.admin = self.session.login(rtype_name='core.user', slug='admin')
        User = self.session.get_rtype('core.user')
        self.user = User(name='Bedevere',
            email='bedevere@camelot.uk', groups=[]
        )


    def test_dehydrate(self):
        """Author is set and hydrate returns correct values."""
        values = {}
        dehydrate(self.user, values)
        name_id = type(self.user).fields['name'].field_id
        email_id = type(self.user).fields['email'].field_id
        self.assertEqual(self.user.creator, self.admin)
        self.assertDictEqual(values['character_varying'], {
            name_id: 'Bedevere',
            email_id: 'bedevere@camelot.uk',
        })
