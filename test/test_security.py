"""Test for ACLs and other security mechanism."""

import unittest

from bdvr.core.parser import ex
from bdvr.core.security import Acl, EDIT, VIEW

class MockResource(object):
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        super(MockResource, self).__init__()

reader = MockResource(name='reader')
admin = MockResource(name='admin')


class TestAcl(unittest.TestCase):
    """Tests for ACLs"""

    def test_acl_conversion(self):
        """ACLs are converted to expressions."""
        acl = Acl([
            ('A', ".name == 'reader'", ".type == 'public'", 'V'),
            ('A', ".groups == `group:reader`", ".type == 'public'", 'V'),
            ('A', ".name == 'admin'", "$True", 'VE'),
            ('D', '$True', '$True', 'VE'),
        ])
        expression = acl.as_expression(reader, VIEW)
        self.assertEqual(expression,
            ex("!($True) | (.type == 'public')")
        )
        self.assertEqual(
            str(acl[0]),
            "ALLOW (.name) == ('reader') (.type) == ('public') {'VIEW'}"
        )
        self.assertEqual(
            str(acl[1]),
            "ALLOW (.groups) == (`group:reader`) "
            "(.type) == ('public') {'VIEW'}"
        )
