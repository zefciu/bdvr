"""Reusable utils for tests."""

def do_migration(module):
    """Migrates the given module."""
    version, code = module.make_migration()
    migration_scope = {}
    exec('\n'.join(code), migration_scope)
    migration_scope['forward'](module.connection)
