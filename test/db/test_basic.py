import psycopg2
import sys
import unittest

from bdvr.core.db import get_eav_version, migrate_eav, Connection, DbStateError
from bdvr.core.db import migrate
from bdvr.core.config import Local

from ._base import ConnectionTest

class TestBasic(ConnectionTest):
    """Tests for some basic operations on db"""

    def test_error(self):
        """get_eav_version propagates exc other 'relation does not exist'"""

        cursor = self.connection.cursor()
        cursor.execute("ALTER TABLE config DROP COLUMN name;")
        with self.assertRaises(psycopg2.ProgrammingError):
            get_eav_version(self.connection)

class TestConnection(unittest.TestCase):
    """Fixture-less tests for connection."""

    def test_wrong_schema(self):
        """You can't connect to schema with wrong EAV version."""
        local = Local('test_local.ini')
        migrate_eav(local.db_conn, towards=0)
        with self.assertRaises(DbStateError):
            connection = Connection(local.db_conn)
        

class TestEntryPoint(unittest.TestCase):
    """Test the calling of entry-point."""

    def setUp(self):
        self.argv = sys.argv
        sys.argv = ['migrate', '-l' 'test_local.ini', '-t', '0']

    def tearDown(self):
        sys.argv = self.argv

    def test_migrate_by_console(self):
        migrate()
        local = Local('test_local.ini')
        self.assertEqual(get_eav_version(psycopg2.connect(local.db_conn)), 0)
