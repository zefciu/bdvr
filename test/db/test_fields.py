from ._base import ConnectionTest
from bdvr.core.db import FieldNotFound

class _TestFields(ConnectionTest):
    """Test for low-level ftype operations."""

    def setUp(self):
        super(_TestFields, self).setUp()
        self.string = self.connection.add_ftype('core.String')
        self.int = self.connection.add_ftype('core.Int')
        self.knight = self.connection.add_rtype('core.Knight', [])
        self.castle = self.connection.add_rtype('core.Castle', [])
        self.knight_name = self.connection.add_field(
            self.knight, self.string, 'name')
        self.knight_age = self.connection.add_field(
            self.knight, self.int, 'age')
        self.castle_name = self.connection.add_field(
            self.castle, self.string, 'name')

    def test_get(self):
        self.assertEqual(
            self.connection.get_field_id(self.knight, 'name'),
            self.knight_name
        )
        self.assertEqual(
            self.connection.get_field_id(self.castle, 'name'),
            self.castle_name
        )

    def test_rm(self):
        self.connection.rm_field(self.castle_name)
        with self.assertRaises(FieldNotFound):
            self.connection.get_field_id(self.castle, 'name'),
        self.assertEqual(
            self.connection.get_field_id(self.knight, 'name'),
            self.knight_name
        )

class TestFieldsReal(_TestFields):
    real_connection = True

class TestFieldsDummy(_TestFields):
    real_connection = False
