from ._base import ConnectionTest
from bdvr.core.db import RtypeNotFound

class _TestRtypes(ConnectionTest):
    """Test for low-level rtype operations."""

    def setUp(self):
        super(_TestRtypes, self).setUp()
        self.animal = self.connection.add_rtype('core.Animal', [])
        self.pythonic = self.connection.add_rtype('core.Pythonic', [])
        self.bird = self.connection.add_rtype('core.Bird', [self.animal])
        self.mammal = self.connection.add_rtype('core.Mammal', [self.animal])
        self.swallow = self.connection.add_rtype('core.Swallow', [self.bird])
        self.african_swallow = self.connection.add_rtype(
            'core.AfricanSwallow', [self.swallow, self.pythonic])
        self.european_swallow = self.connection.add_rtype(
            'core. EuropeanSwallow', [self.swallow, self.pythonic])
        self.tiger = self.connection.add_rtype(
            'core.Tiger', [self.mammal, self.pythonic])

    def test_ancestors(self):
        """Method get_rtype_ancestors returns correct set of ancestors."""
        self.assertSetEqual(
            set(self.connection.get_rtype_ancestors(self.european_swallow)),
            {self.european_swallow, self.swallow, self.bird, self.animal,
            self.pythonic}
        )

    def test_get_id(self):
        """You can retrieve id by name."""
        self.assertEqual(self.connection.get_rtype_id('core.Bird'), self.bird)

    def test_rm_rtype(self):
        """Remove the rtype."""
        self.connection.rm_rtype(self.bird)
        with self.assertRaises(RtypeNotFound):
            self.connection.get_rtype_id('core.Bird')

class TestRtypesReal(_TestRtypes):
    real_connection = True

class TestRtypesDummy(_TestRtypes):
    real_connection = False
