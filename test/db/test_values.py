from ._base import ConnectionTest
from bdvr.core.db import ResourceNotFound


class _TestValues(ConnectionTest):
    """Test for low-level resource operations."""

    def setUp(self):
        super(_TestValues, self).setUp()
        self.knight = self.connection.add_rtype('core.Knight', [])
        self.string = self.connection.add_ftype('core.String')
        self.m2m = self.connection.add_ftype('core.m2m')
        self.int = self.connection.add_ftype('core.Int')
        self.knight_name = self.connection.add_field(
            self.knight, self.string, 'name')
        self.knight_nickname = self.connection.add_field(
            self.knight, self.string, 'nickname')
        self.knight_age = self.connection.add_field(
            self.knight, self.int, 'age')
        self.knight_friend = self.connection.add_field(
            self.knight, self.m2m, 'friend')
        self.galahad = self.connection.create_resource(
            self.knight, 'galahad-the-pure', 'APPLIED'
        )
        self.bedevere = self.connection.create_resource(
            self.knight, 'bedevere-the-wise', 'APPLIED'
        )
        self.connection.set_values(
            self.galahad, {
                'character_varying': {
                    self.knight_name: 'Galahad',
                    self.knight_nickname: 'The Pure',
                }, 
                'integer': {
                    self.knight_age: 30,
                },
                'm2m': {
                    self.knight_friend: [self.bedevere],
                },
            },
        )
        self.connection.set_values(
            self.bedevere, {
                'character_varying': {
                    self.knight_name: 'Bedevere',
                    self.knight_nickname: 'The Wise',
                }, 
                'integer': {
                    self.knight_age: 35,
                }
            },
        )

    def test_get_values(self):
        """Get values."""
        galahad_data = self.connection.get_values(self.galahad)
        bedevere_data = self.connection.get_values(self.bedevere)
        self.assertEqual(galahad_data['integer'][self.knight_age], 30)
        self.assertEqual(
            galahad_data['m2m'][self.knight_friend],
            [self.bedevere]
        )
        self.assertEqual(
            bedevere_data['character_varying'][self.knight_name],
            'Bedevere'
        )

    def test_set_values(self):
        """Overwriting values."""
        self.connection.set_values(
            self.galahad, {
                'character_varying': {
                    self.knight_nickname: 'The Dirty',
                }, 
                'integer': {
                    self.knight_age: 31,
                },
                'm2m': {
                    self.knight_friend: [],
                },
            },
        )
        galahad_data = self.connection.get_values(self.galahad)
        bedevere_data = self.connection.get_values(self.bedevere)
        self.assertEqual(galahad_data['integer'][self.knight_age], 31)
        self.assertEqual(
            galahad_data['character_varying'][self.knight_name],
            'Galahad'
        )
        self.assertEqual(
            galahad_data['character_varying'][self.knight_nickname],
            'The Dirty'
        )
        self.assertEqual(galahad_data['m2m'], {})


class TestValuesReal(_TestValues):
    real_connection = True


class TestValuesDummy(_TestValues):
    real_connection = False
