from ._base import ConnectionTest
from bdvr.core.db import ResourceNotFound

class _TestResources(ConnectionTest):
    """Test for low-level value operations."""

    def setUp(self):
        super(_TestResources, self).setUp()
        self.knight = self.connection.add_rtype('core.Knight', [])
        self.castle = self.connection.add_rtype('core.Castle', [])
        self.galahad = self.connection.create_resource(
            self.knight, 'galahad-the-pure', 'APPLIED'
        )
        self.bedevere = self.connection.create_resource(
            self.knight, 'bedevere-the-wise', 'APPLIED'
        )
        self.camelot = self.connection.create_resource(
            self.castle, 'camelot', 'PROJECTED'
        )

    def test_get_by_id(self):
        """Get resource by id."""
        self.assertEqual(
            tuple(self.connection.get_resource(id=self.bedevere)), 
            (self.bedevere, self.knight, 'bedevere-the-wise', 'APPLIED')
        )

    def test_get_by_slug(self):
        """Get resource by slug."""
        self.assertEqual(
            self.connection.get_resource(
                rtype_id=self.knight, slug='bedevere-the-wise'
            ), 
            (self.bedevere, self.knight, 'bedevere-the-wise', 'APPLIED')
        )

    def test_by_slug_only(self):
        """You can't get by untyped slug."""
        with self.assertRaises(TypeError):
            self.connection.get_resource(slug='bedevere-the-wise')

    def test_delete(self):
        """Delete a resource."""
        self.connection.delete_resource(self.bedevere)
        with self.assertRaises(ResourceNotFound):
            self.connection.get_resource(id=self.bedevere)

    def test_update(self):
        """Various updates of resource."""
        self.connection.update_resource(self.bedevere, state='ACCEPTED')
        self.assertEqual(
            tuple(self.connection.get_resource(id=self.bedevere)), 
            (self.bedevere, self.knight, 'bedevere-the-wise', 'ACCEPTED')
        )
        self.connection.update_resource(
            self.bedevere, state='REJECTED', slug='bedevere-the-foolish',
        )
        self.assertEqual(
            tuple(self.connection.get_resource(id=self.bedevere)),
            (self.bedevere, self.knight, 'bedevere-the-foolish', 'REJECTED')
        )


class TestResourcesReal(_TestResources):
    real_connection = True


class TestResourcesDummy(_TestResources):
    real_connection = False
