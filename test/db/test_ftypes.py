from ._base import ConnectionTest
from bdvr.core.db import FtypeNotFound

class _TestFtypes(ConnectionTest):
    """Test for low-level ftype operations."""

    def setUp(self):
        super(_TestFtypes, self).setUp()
        self.string = self.connection.add_ftype('core.String')
        self.int = self.connection.add_ftype('core.Int')

    def test_get(self):
        self.assertEqual(
            self.connection.get_ftype_id('core.String'), self.string
        )

    def test_rm(self):
        self.connection.rm_ftype(self.string)
        with self.assertRaises(FtypeNotFound):
            self.connection.get_ftype_id('core.String')

class TestFtypesReal(_TestFtypes):
    real_connection = True

class TestFtypesDummy(_TestFtypes):
    real_connection = False
