import unittest

from bdvr.core.config import Local
from bdvr.core.db import migrate_eav, Connection

from ..mem_connection import MemConnection


class ConnectionTest(unittest.TestCase):
    """A test case that creates a database connection."""

    real_connection = True

    def setUp(self, *args, **kwargs):
        """Create the test connection and migrate EAV."""
        if self.real_connection:
            self.local = Local('test_local.ini')
            migrate_eav(self.local.db_conn)
            self.connection = Connection(self.local.db_conn)
        else:
            self.connection = MemConnection()
        super(ConnectionTest, self).setUp(*args, **kwargs)

    def tearDown(self, *args, **kwargs):
        """Migrate the EAV to 0 (destroys all data)."""
        if self.real_connection:
            self.connection.rollback()
            migrate_eav(self.local.db_conn, towards=0)
        super(ConnectionTest, self).tearDown(*args, **kwargs)
