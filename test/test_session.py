"""Tests for the session object."""

import unittest

from bdvr.core.session import Session
from bdvr.core.module import load_module

from .mem_connection import MemConnection


class TestSession(unittest.TestCase):
    """Test a working session."""

    def setUp(self):
        self.connection = MemConnection()
        self.module = load_module('core', self.connection)
        version, code = self.module.make_migration()
        migration_scope = {}
        exec('\n'.join(code), migration_scope)
        migration_scope['forward'](self.connection)
        Session.prepare(self.connection, self.module)
        self.session = Session(self.connection, self.module)
        self.admin = self.session.login(rtype_name='core.user', slug='admin')

    def test_create_user(self):
        """Creating a user"""
        User = self.session.get_rtype('core.user')
        user = User(
            name='bedevere', email='bedevere@camelot.uk', groups=[],
        )

    def test_commit(self):
        """Commit data to connection."""
        User = self.session.get_rtype('core.user')
        user = User(
            name='bedevere', email='bedevere@camelot.uk', groups=[]
        )
        self.session.commit()
        session2 = Session(self.connection, self.module)
        session2.login(rtype_name='core.user', slug='admin')
        user = session2.get_resource('core.user:bedevere') 
        self.assertEqual(user.name, 'bedevere')
