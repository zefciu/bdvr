from bidict import bidict

import indexed_list as il

from bdvr.core.db import RtypeNotFound, FtypeNotFound, FieldNotFound
from bdvr.core.db import ResourceNotFound


class ResourcesTable(il.Table):
    """In-memory table that keeps resources data."""
    resource_id = il.UniqueColumn()
    rtype_id = il.MultiColumn()
    slug = il.MultiColumn()
    state = il.MultiColumn()
    rtype_slug = il.UniqueAttributeIndex('rtype_id', 'slug')


class MemConnection(object):
    """Dummy connection storing model and values in memory."""

    def __init__(self):
        self.modules = {}
        self.last_rtype = 0
        self.rtype_seq = 0
        self.rtypes = bidict()
        self.rtypes_parents = {}
        self.ftype_seq = 0
        self.ftypes = bidict()
        self.field_seq = 0
        self.fields = bidict()
        self.fields_ftypes = {}
        self.resource_seq = 0
        self.resources = ResourcesTable()
        self.resource_values = {}

    def commit(self):
        pass # Hope we won't need transactions

    @property
    def connection_string(self):
        """Dummy connection string."""
        return '__memory__'

    def get_module_version(self, module):
        return self.modules.get(module, 0)

    def set_module_version(self, module, version):
        self.modules[version] = module

    def get_rtype_id(self, name):
        """Get the id of rtype with given name."""
        try:
            return self.rtypes[:name]
        except KeyError:
            raise RtypeNotFound(name)

    def get_rtype_ancestors(self, id):
        """Return all the unique ancestors of a given rtype including itself.
        Note: this is not to be used as MRO. Let the python count MRO's
        """
        result = set([id])
        for ancestor in self.rtypes_parents.get(id, []):
            result |= self.get_rtype_ancestors(ancestor)
        return result

    def add_rtype(self, name, parents):
        """Add an rtype.
        :param name: The qualified name of rtype
        :param parents: The list of direct parents of this rtype
        :return: The id of newly created rtype
        """
        self.rtype_seq += 1
        self.rtypes[self.rtype_seq] = name
        self.rtypes_parents[self.rtype_seq] = parents
        return self.rtype_seq

    def rm_rtype(self, rtype_id):
        """Remove rtype.
        :param rtype_id: The id of rtype to remove
        """
        del self.rtypes[rtype_id]
        del self.rtypes_parents[rtype_id]

    def add_ftype(self, name):
        """Add ftype.
        :param name: The qualified name of ftype
        :return: The id of ftype
        """
        self.ftype_seq += 1
        self.ftypes[self.ftype_seq] = name
        return self.ftype_seq

    def get_ftype_id(self, name):
        """Get the ftype id.
        :param name: The qualified name of ftype
        :return: The id of ftype
        """
        try:
            return self.ftypes[:name]
        except KeyError:
            raise FtypeNotFound(name)

    def rm_ftype(self, ftype_id):
        """Remove the ftype.
        :param ftype_id: The id of ftype
        """
        del self.ftypes[ftype_id]

    def add_field(self, rtype_id, ftype_id, name):
        """Add a field.
        :param ftype_id: The id of ftype.
        :param rtype_id: The id of rtype.
        :param name: The name of field.
        :return: The id of a field."""
        self.field_seq += 1
        self.fields[self.field_seq] = (rtype_id, name)
        self.fields_ftypes[self.field_seq] = ftype_id
        return self.field_seq

    def get_field_id(self, rtype_id, name):
        """Get the ftype id.
        :param rtype_id: The id of rtype
        :param name: The name of field
        :return: The id of field
        """
        try:
            return self.fields[:(rtype_id, name)]
        except KeyError:
            raise FieldNotFound(name)

    def rm_field(self, field_id):
        """Remove the ftype.
        :param field_id: The id of field
        """
        del self.fields[field_id]

    def create_resource(self, rtype_id, slug, state):
        """Create a resource
        :param rtype: The id of rtype
        :param slug: The slug of a new resource
        :param state: The state of a new resource
        :return: The id of a resource"""
        self.rtype_seq += 1
        self.resources.append((
            self.rtype_seq, rtype_id, slug, state
        ))
        return self.rtype_seq

    def get_resource(self, rtype_id=None, id=None, slug=None):
        """Get a resource by either id or slug.
        :param rtype_id: The id of rtype
        :param id: The id of resource
        :param slug: The slug of resource
        :return: The tuple (resource_id, rtype_id, slug, state)"""
        if not ((rtype_id and slug) or id):
            raise TypeError('Either rtype_id and slug or id is required')
        if id:
            try:
                return self.resources.resource_id[id]
            except KeyError:
                raise ResourceNotFound(id)
        else:
            try:
                return self.resources.rtype_slug[(rtype_id, slug)]
            except KeyError:
                raise ResourceNotFound(rtype_id, slug)

    def update_resource(self, resource_id, slug=None, state=None):
        """Update the slug or state of a resource
        :param resource_id: The id of rtype
        :param slug: The new slug (defaults to None - don't update
        :param state: The new state (defaults to None - don't update"""
        resource = self.resources.resource_id[resource_id]
        index = self.resources.index(resource)
        resource_id, rtype_id, old_slug, old_state = resource
        slug = slug if slug is not None else old_slug
        state = state if state is not None else old_state
        self.resources[index] = (resource_id, rtype_id, slug, state)


    def delete_resource(self, resource_id):
        """Delete the resource.
        :param resource_id: The id of resource
        """
        del self.resources[
            self.resources.index(self.resources.resource_id[resource_id])
        ]

    def set_values(self, resource_id, values):
        """Set the values on resource.
        :param resource_id: The id of this resource
        :param values: The mapping of values
        Example values:
        {
            'character_varying': {
                1: 'Galahad',
                2: 'The Pure,
            },
            'integer': {
                3: 30,
            }
        }
        (1, 2, 3) are field_ids
        """
        self.resource_values.setdefault(resource_id, {})
        for datatype, field_values in values.items():
            self.resource_values[resource_id].setdefault(datatype, {})
            self.resource_values[resource_id][datatype].update(field_values)
            self.resource_values[resource_id][datatype] = dict(
                (k, v) for (k, v) in
                self.resource_values[resource_id][datatype].items()
                if v is not None and v != []
            )
    
    def get_values(self, resource_id):
        """Get the values for a given resource."""
        return self.resource_values[resource_id]
