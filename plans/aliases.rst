-----------------------------------------------
Aliases
-----------------------------------------------

An alias is a way to address a field on a context rtype or on some remote rtype
connected to the context rtype via relation fields.

An alias is specific and constant for the rtype. After it is computed it is
stored on that rtype. The rtype will therefore always return the same alias
for the same path.

An alias consists of:

* a JOIN clause
* an alias name (which also appears in a JOIN clause)
* an output context

An alias of a path that contains more than one segment will have a parent
alias.

A way to create an alias is implemented by the connection. The field should
contain information whether it is scalar or relational and what table should be
used to create the join.

While creating a new alias, the following are used:
* A context (which is either a context resource or resource returned by parent)
* A sequencer of names

The sequencer is a simple object that is able to fetch a non-conflicting names
in a form of ``name_base`` + ``integer``

During search the JOIN clauses should be rendered int the SQL query without
repeating and parents before children.

The alias names will then be used in the WHERE clause.
