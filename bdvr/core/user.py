"""Imperative part of user resource."""

from bdvr.core.resource import Resource


class User(Resource):
    """User"""

    def authenticate(self, request):
        return True
