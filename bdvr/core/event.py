"""Event system."""

class Subscription(object):
    """A subscription is bound to an rtype. The subscriptions are scanned
    when an event is fired and the matching subscriptions are run.

    The subscription is before all tested by its Event class. It can however
    also be parametrised.
    """

    def __init__(self, Event, handler, **kwargs):
        self.Event = Event
        self.handler = handler
        self.kwargs = kwargs

    def check(self, event):
        if not isinstance(event, self.Event):
            return False
        for k, v in self.kwargs.items():
            if getattr(event, k, None) != v:
                return False
        return True

    def __call__(self, context, event):
        self.handler(context, event)

class Event(object):
    """An event is an object that gets passed to each handler. It is a
    data container that handlers can read and modify."""

    def __init__(self, context, **kwargs):
        self.context = context
        for k, v in kwargs.items():
            setattr(self, k, v)
        super(Event, self).__init__()

class HaltableEvent(Event):
    """An event that can be halted. This is a class of beforeX events where X
    can be yet prevented by calling .halt() on the event object."""

    def __init__(self, context, **kwargs):
        self._halted = False
        super(HaltableEvent, self).__init__(context, **kwargs)

    def halt(self):
        self._halted = True

    @property
    def halted(self):
        return self._halted

class OnTransition(Event):
    """Fired after a transition occurs. It is a parametrised event
    with the initial and target states as parameters."""

    def __init__(self, context, from_=None, to=None, **kwargs):
        self.from_ = from_
        self.to = to
        super(OnTransition, self).__init__(
            context, from_=from_, to=to, **kwargs
        )
