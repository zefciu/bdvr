"""Resource and resource accessor definition."""

from collections import OrderedDict
import weakref

from bdvr.core.alias import Sequencer
from bdvr.core.db import RtypeNotFound, FieldNotFound
from bdvr.core.event import Subscription, OnTransition
from bdvr.core.field import Field
from bdvr.core.security import can, Unauthorised, VIEW, EDIT, Acl
from bdvr.core.workflow import Workflow


READ = 'READ'
WRITE = 'WRITE'


class Rtype(type):
    """Metaclass for resource."""
    def __init__(cls, clsname, bases, dict_):
        cls.own_fields = {}
        if dict_:
            for k, v in dict_.items():
                if isinstance(v, Field):
                    cls.own_fields[k] = v
            cls.fields = cls.merge_fields(bases, cls.own_fields)
        else: # Merging declarative and imperative components
            cls.fields = {}
            for base in bases:
                cls.own_fields.update(base.own_fields)
                cls.fields.update(base.fields)
        cls.__acl__ = cls.merge_sequence(bases, dict_, '__acl__', Acl)
        cls.__workflow__ = cls.merge_sequence(
            bases, dict_, '__workflow__', Workflow
        )
        if 'module' in dict_:
            cls.module = dict_['module']
        cls.synced = False
        cls.subscriptions = []
        cls.aliases = {}
        cls.sequencer = Sequencer()

    def on(cls, Event, handler, **kwargs):
        cls.subscriptions.append(Subscription(Event, handler, **kwargs))

    def add_field(cls, field):
        """Add a new field dynamically."""
        cls.own_fields[field.name] = field
        cls.fields[field.name] = field
        field.rtype = cls

    def merge_fields(cls, bases, new_fields):
        """Prepare a consolidated list of fields for this class."""
        result = OrderedDict()
        for field_dict in (base.fields for base in reversed(bases)):
            result.update(field_dict)
        result.update(new_fields)
        return result

    def merge_sequence(cls, bases, dict_, attr, result_cls):
        result = []
        result += iter(dict_.get(attr, []))
        for base in bases:
            if hasattr(base, attr):
                result += iter(getattr(base, attr))
        return result_cls(result)

    @property
    def bdvr_qual_name(cls):
        """The full, unique name of this rtype."""
        return '.'.join([cls.module.name, cls.__name__])

    def sync(cls, connection):
        """Find itself in the database and set rtype_id."""
        cls.rtype_id = connection.get_rtype_id(cls.bdvr_qual_name)
        cls.synced = True
        for field in cls.fields.values():
            field.sync(connection)

    def setup_relations(cls, module):
        for field in list(cls.fields.values()):
            field.setup_relations(module)

    def get_migration_code(cls, connection):
        fw_result = []
        bk_result = []
        try:
            cls.sync(connection)
        except RtypeNotFound:
            created = True
            fw_result.append(
                "RTYPES['{qname}'] = connection.add_rtype('{qname}', [])".
                    format(
                        qname=cls.bdvr_qual_name
                    )
            )
            bk_result += [
                "rtype = connection.get_rtype_id('{0}')".format(
                    cls.bdvr_qual_name
                ),
                'connection.rm_rtype(rtype)',
            ]
            return fw_result, bk_result
        except FieldNotFound:
            pass # The fields are changed, the module should take care
        created = False
        fw_result.append(
            "RTYPES['{qname}'] = connection.get_rtype_id('{qname}')".format(
                qname=cls.bdvr_qual_name
            )
        )
        return fw_result, bk_result



class ResourceManipulator:
    """An object that lives in the resource and allows to perform operations
    other than field access"""

    def __init__(self, resource):
        self.resource = resource

    def check_workflow(self):
        """Check the modifications on this field and perform a transition
        if necessary."""
        before, after = self.resource.__workflow__.perform(self.resource)
        if before != after:
            self.fire(OnTransition, from_=before, to=after)

    def process_data(self, data):
        """Load a data dictionary and perform the workflow transition."""
        raise NotImplementedError

    def dehydrate(self, values):
        """Dehydrates the resource.
        :param values: The values dictionary that would be passed to connection
        """
        for field in type(self.resource).fields.values():
            field.dehydrate(self.resource, values)

    def hydrate(self, values):
        """Hydrates the resource.
        :param values: The values dictionary that was get from connection
        """
        for field in type(self.resource).fields.values():
            field.hydrate(self.resource, values)

    def fire(self, Event, **kwargs):
        """Fire an event on the resource.
        :param Event: The event class.
        "return: The event object after being passed through all the handlers.
        """
        event = Event(self.resource, **kwargs)
        subscriptions = []
        for cls in type(self.resource).__mro__:
            if issubclass(cls, Resource):
                for subscription in cls.subscriptions:
                    if subscription.check(event):
                        subscriptions.append(subscription)
        for subscription in subscriptions:
            subscription(self.resource, event)
        return event


class Resource(metaclass=Rtype):
    """Basic resource. It exposes no methods, only the fields and
    __manipulator__."""


    def __new__(cls, *args, **kwargs):
        try:
            session = cls.session
        except AttributeError:
            raise TypeError("""This rtype is not added to any session.""")
        self = super(Resource, cls).__new__(cls)
        self.__modes__ = set()
        self.__session__ = weakref.proxy(session)
        self.__actor__ = session.actor
        for perm, mode in [(VIEW, READ), (EDIT, WRITE)]:
            if self.__actor__ is None:
                self.__modes__ = {READ}
            if can(self.__actor__, perm, self):
                self.__modes__.add(mode)
        self.__manipulator__ = ResourceManipulator(self)
        self.__id__ = None 
        return self

    def __init__(self, **kwargs):
        self.__state__ = '__NIHILUM__'
        for k, field in type(self).fields.items():
            if k in kwargs:
                setattr(self, k, kwargs[k])
            else:
                setattr(self, k, field.get_default(self))

        if self.__actor__ is None:
            # This is probably an actor itself. You can't edit your
            # own resource unless authorized
            self.__modes__.discard = {WRITE}
        else:
            check_workflow(self)
        self.__session__.add(self)

    @property
    def __slug__(self):
        if hasattr(self, 'slug'):
            return self.slug
        elif hasattr(self, 'name'):
            return self.name

    def __str__(self):
        # if hasattr(self, 'title'):
        #     return self.title
        # elif hasattr(self, 'name'):
        #     return self.name
        # else:

        return (
            self.__dict__.get('title') or
            self.__dict__.get('name') or
            self.__dict__.get('slug') or
            'unknown resource'
        )

    def __getattribute__(self, key):
        if key.startswith('__') and key.endswith('__'):
            return super(Resource, self).__getattribute__(key)
        if READ not in self.__modes__:
            raise Unauthorised(self.__actor__, VIEW, self)
        return super(Resource, self).__getattribute__(key)

    def __setattr__(self, key, value):
        if key.startswith('__') and key.endswith('__'):
            return super(Resource, self).__setattr__(key, value)
        if WRITE not in self.__modes__:
            raise Unauthorised(self.__actor__, EDIT, self)
        field = type(self).fields[key]
        if not can(self.__actor__, EDIT, field):
            raise Unauthorised(self.__actor__, EDIT, field)
        super(Resource, self).__setattr__(key, value)
        self.__manipulator__.dirty = True

    def __delattr__(self, key):
        raise NotImplementedError


def on_create(context, event, **kwargs):
    """Fired on any transition from __NIHILUM__. Sets 'creator'."""
    if context.__session__.actor.__id__ is not None:
        context.creator = context.__session__.actor


Resource.on(OnTransition, on_create, from_='__NIHILUM__')
            

# This is the proper way of calling manipulator methods:

def check_workflow(resource):
    return resource.__manipulator__.check_workflow()

def process_data(resource, data):
    return resource.__manipulator__.process_data(data)

def dehydrate(resource, values):
    return resource.__manipulator__.dehydrate(values)

def hydrate(resource, values):
    return resource.__manipulator__.hydrate(values)

def fire(resource, Event, **kwargs):
    return resource.__manipulator__.fire(Event, **kwargs)
