"""Authentication tools.
BDVR uses AuthTkt authentication policy. It would however need a callback to
fetch the user object and also some methods to login. The login process will be
performed outside the pyramid auth system, calling only the ``remember`` method
when necessary.
"""

from collections import OrderedDict
import pkg_resources

from anthrax.container.base import ContainerMeta
from anthrax.container.form import Form
from anthrax.field.text import TextField
from anthrax.field.action import HttpSubmit
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember

from bdvr.core.db import ResourceNotFound


class BdvrAuthenticationPolicy(AuthTktAuthenticationPolicy):
    def effective_principals(self, request):
        username = self.authenticated_userid(request)
        if username:
            try:
                user = request.bdvr_session.login(
                    rtype_name='user',
                    slug=username,
                )
            except ResourceNotFound:
                return None
            request.user = user
            return user
        else:
            return None


class TrustPlugin(object):
    """Login plugin for testing purposes. It trusts any username specified."""

    def __init__(self, settings, *args, **kwargs):
        super(TrustPlugin, self).__init__(*args, **kwargs)

    def get_fields(self, fields):
        if 'trust_login' not in fields:
            fields['trust_login'] = TextField(label='Username (test mode)')
            fields['trust_submit'] = HttpSubmit(label='Login')

    def login(self, form):
        if 'trust_submit' in form:
            return form['trust_login']


class LoginView(object):
    """The view invoked to login user. It is based on plugins."""

    def __init__(self, settings, *args, **kwargs):
        self.plugins = []
        for plugin_name in settings['bdvr.login_methods'].split():
            try:
                Plugin = next(pkg_resources.iter_entry_points(
                    'bdvr.login_plugin', plugin_name
                )).load()
            except StopIteration():
                raise ValueError('Cannot find plugin {0}'.format(plugin_name))
            self.plugins.append(Plugin(settings))
        super(LoginView, self).__init__(*args, **kwargs)

    def __call__(self, context, request):
        form = self.make_form()
        form.kwargs['method'] = 'POST'
        if request.POST:
            form.__raw__ = request.POST
            if form.__valid__:
                for plugin in self.plugins:
                    userid = plugin.login(form)
                    if userid is not None:
                        headers = remember(request, userid)
                        return HTTPFound(location='/', headers=headers)
        return {'form': form.render()}

    def make_form(self):
        """Compiles the form to be displayed."""
        fields = OrderedDict()
        fields['__frontend__'] = 'dojo'
        for plugin in self.plugins:
            plugin.get_fields(fields)
        LoginForm = ContainerMeta('LoginForm', (Form,), fields)
        return LoginForm()
