CREATE TABLE config (
    name character varying PRIMARY KEY,
    value character varying
) WITHOUT OIDS;

CREATE TABLE modules (
    name character varying PRIMARY KEY,
    version integer
) WITHOUT OIDS;

INSERT INTO config VALUES('eav_version', '001');

CREATE TABLE rtypes (
    rtype_id serial PRIMARY KEY,
    name character varying UNIQUE
) WITHOUT OIDS;

CREATE TABLE rtypes_inheritance (
    parent_id integer REFERENCES rtypes(rtype_id) ON DELETE CASCADE,
    child_id integer REFERENCES rtypes(rtype_id) ON DELETE CASCADE
) WITHOUT OIDS;

CREATE TABLE ftypes (
    ftype_id serial PRIMARY KEY,
    name character varying UNIQUE
) WITHOUT OIDS;

CREATE TABLE fields (
    field_id serial PRIMARY KEY,
    rtype_id integer REFERENCES rtypes(rtype_id) ON DELETE CASCADE,
    ftype_id integer REFERENCES ftypes(ftype_id) ON DELETE CASCADE,
    name character varying,
    UNIQUE (rtype_id, name)
) WITHOUT OIDS;

CREATE TABLE resources (
    resource_id bigserial PRIMARY KEY,
    rtype_id integer REFERENCES rtypes(rtype_id) ON DELETE CASCADE,
    slug character varying,
    state character varying,
    UNIQUE (rtype_id, slug) 
) WITHOUT OIDS;

CREATE TABLE values_character_varying (
    resource_id bigint REFERENCES resources(resource_id) ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    value character varying,
    PRIMARY KEY (resource_id, field_id)

) WITHOUT OIDS;

CREATE TABLE values_text (
    resource_id bigint REFERENCES resources(resource_id) ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    value text,
    PRIMARY KEY (resource_id, field_id)

) WITHOUT OIDS;

CREATE TABLE values_integer (
    resource_id bigint REFERENCES resources(resource_id) ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    value integer,
    PRIMARY KEY (resource_id, field_id)

) WITHOUT OIDS;

CREATE TABLE values_boolean (
    resource_id bigint REFERENCES resources(resource_id) ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    value boolean,
    PRIMARY KEY (resource_id, field_id)

) WITHOUT OIDS;

CREATE TABLE relations_m21 (
    first_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    second_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    PRIMARY KEY (field_id, first_resource_id, second_resource_id),
    UNIQUE (field_id, second_resource_id)
) WITHOUT OIDS;

CREATE TABLE relations_12m (
    first_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    second_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    PRIMARY KEY (field_id, first_resource_id, second_resource_id),
    UNIQUE (field_id, first_resource_id)
) WITHOUT OIDS;

CREATE TABLE relations_m2m (
    first_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    second_resource_id bigint REFERENCES resources(resource_id)
        ON DELETE CASCADE,
    field_id integer REFERENCES fields(field_id) ON DELETE CASCADE,
    PRIMARY KEY (field_id, first_resource_id, second_resource_id)
) WITHOUT OIDS;
