-----------------------------------
Eav migrations
-----------------------------------

This directory contains pure-SQL files for migrations of EAV schema.

Currently only one migration is here 000-001.sql which means creating the necessary
schema from empty database.
