"""Miscelanous fields."""

from bdvr.core.field import Field


class Boolean(Field):
    """Boolean type."""

    def dehydrate(self, resource, values):
        values.setdefault('boolean', {})[
            self.field_id
        ] = resource.__dict__[self.name]

    def hydrate(self, resource, values):
        resource.__dict__[self.name] = values[
            'boolean'
        ].get(self.field_id, None)

class Action(Field):
    """Actions are not saved to the database. Their value exists only to
    resolve the workflow."""

    def dehydrate(self, resource, values):
        pass

    def hydrate(self, resource, values):
        pass
