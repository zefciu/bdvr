"""All the basic relationship objects."""
import abc
from collections.abc import MutableSet, Iterable
import weakref

from bdvr.core.field import Field
from bdvr.core.parser import ex
from bdvr.core.resource import Rtype, Resource
from bdvr.core.session import ghost


class RelatedSet(MutableSet):
    """The lazy loading set of related resources."""

    # No caching here. The session should cache resources.

    def __init__(self, values, rtype, owner):
        self.owner = weakref.proxy(owner)
        self.rtype = rtype
        self._elements = set(self._add_value(value) for value in values)

    @property
    def ids(self):
        """Iterate over ids of the resources in set."""
        result = []
        for el in self._elements:
            if isinstance(el, int):
                result.append(el)
            else:
                rtype_name, slug = el
                result.append(self.owner.__session__.get_resource(
                    rtype_name=rtype_name, slug=slug,
                ).__id__)
        return result

    def _add_value(self, value):
        """Recognise if resource, id or slug was passed and return the value
        that should be added to set."""
        if isinstance(value, int):
            return value
        elif isinstance(value, str):
            resource = self.owner.__session__.get_resource(
                rtype_id=self.rtype.rtype_id, slug=value
            )
        elif isinstance(value, Resource):
            resource = value
        else:
            raise ValueError('You can add ids, slugs or resources only')
        if ghost(resource):
            return type(resource).__name__, resource.__slug__
        else:
            return resource.__id__

    def __contains__(self, resource):
        return (
            resource.__id__ in self._elements or
            (type(resource).name, resource.__slug__) in self._elements
        )

    def __iter__(self):
        for el in self._elements:
            if isinstance(el, int):
                yield self.owner.__session__.get_resource(
                    identification=el
                )
            elif isinstance(el, tuple):
                rtype_name, slug = el
                yield self.owner.__session__.get_resource(
                    rtype_name=rtype_name, slug=slug,
                )

    def __len__(self):
        return len(self._elements)

    def add(self, resource):
        if resource in self:
            return
        self._elements.add(self._add_value(resource))

    def discard(self, resource):
        self._elements.discard(resource.__id__)
        self._elements.discard((type(resource).name, resource.__slug__))


class _RelationField(Field):
    """Base, abstract relation field."""

    is_relation = True

    def __init__(self, foreign, backref=None, **kwargs):
        self.foreign = foreign
        self.backref = backref
        super(_RelationField, self).__init__(**kwargs)

    def setup_relations(self, module):
        """This method will be called after every rtype and field in the module
        is set up."""
        if isinstance(self.foreign, Rtype):
            self.ForeignRtype = self.foreign
        else:
            self.ForeignRtype = module.get_rtype(self.foreign)
        super(_RelationField, self).setup_relations(module)


class _ManyToX(_RelationField):
    """A field that returns a set of values."""

    def __set__(self, resource, value):
        if not isinstance(value, Iterable):
            raise ValueError('This field should be set with iterable')
        resource.__dict__[self.name] = RelatedSet(
            value,
            self.ForeignRtype,
            resource,
        )

    def hydrate(self, resource, values):
        resource.__dict__[self.name] = RelatedSet(
            values[type(self).values_key].get(self.field_id, []),
            self.ForeignRtype,
            resource,
        )

    def dehydrate(self, resource, values):
        values.setdefault(type(self).values_key, {})[
            self.field_id
        ] = resource.__dict__[self.name].ids

    def get_default(self, resource):
        return RelatedSet(set(), self.ForeignRtype, resource)

    def get_possible(self, session):
        """Return the search of all possible resources that can be bound using
        this field."""
        return session.search(self.ForeignRtype, ex('$True')) # TODO ACL


class ManyToMany(_ManyToX):
    """The field that represents the 'master' side of a m2m relation."""

    # Reversed = _SlaveManyToMany
    values_key = 'm2m'
    table_name = 'relations_m2m'


class OneToMany(_RelationField):
    """The 'one' side of a m21 relation. It is always the 'slave' side.
    """

    def hydrate(self, resource, values):
        ids = values['12m'].get(self.field_id, [])
        if ids:
            resource.__dict__[self.name] = resource.__session__.get_resource(
                ids[0]
            )
        else:
            resource.__dict__[self.name] = None

    def dehydrate(self, resource, values):
        if resource.__dict__[self.name] is not None:
            values.setdefault('12m', {})[self.field_id] = [
                resource.__dict__[self.name].__id__,
            ]
        else:
            values.setdefault('12m', {})[self.field_id] = []


class ManyToOne(_ManyToX):
    """The 'many' side of m21 relation. It is alway the 'master' side."""

    values_key = 'm21'
