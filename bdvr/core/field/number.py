"""Numeric fields."""

from bdvr.core.field import Field


class Integer(Field):
    """Integer field."""

    def dehydrate(self, resource, values):
        values.setdefault('integer', {})[
            self.field_id
        ] = resource.__dict__[self.name]
