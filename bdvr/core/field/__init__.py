import abc
import weakref

from bdvr.core.db import FtypeNotFound, FieldNotFound, RtypeNotFound


class Field(object):
    """Field base class."""

    __metaclass__ = abc.ABCMeta
    is_relation = False

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        super(Field, self).__init__()

    def setup_relations(self, module):
        """The place that can contain any logic that needs to be done after
        all the rtypes and fields are created."""

    @abc.abstractmethod
    def dehydrate(self, resource, values):
        """Dehydrate the data specific for this field.
        :param resource: The resource with data
        :param values: The dictionary that will be passed to ``set_values``
        """

    def get_default(self, resource):
        """Return the default value for this field."""
        return None

    @property
    def rtype(self):
        """The rtype of this field."""
        return self._rtype

    @rtype.setter
    def rtype(self, value):
        self._rtype = weakref.proxy(value)

    @classmethod
    def get_ftype_bdvr_qual_name(cls):
        """:return" The qualified name of the ftype."""
        return '.'.join([cls.module.name, cls.__name__])
 
    @classmethod
    def sync_ftype(cls, connection):
        """Perform the syncing of the ftype."""
        cls.ftype_id = connection.get_ftype_id(cls.get_ftype_bdvr_qual_name())

    @classmethod
    def get_ftype_migration_code(cls, connection):
        """Get the migration code (fw, bk) for the ftype."""
        fw_result = []
        bk_result = []
        try:
            cls.sync_ftype(connection)
            fw_result.append(
                "FTYPES['{qname}'] = connection.get_ftype_id('{qname}')".format(
                    qname=cls.get_ftype_bdvr_qual_name()
                )
            )
        except FtypeNotFound:
            fw_result.append(
                "FTYPES['{qname}'] = connection.add_ftype('{qname}')".format(
                    qname=cls.get_ftype_bdvr_qual_name()
                )
            )
            bk_result += [
                "ftype = connection.get_ftype_id('{0}')".format(
                    cls.get_ftype_bdvr_qual_name()
                ),
                "connection.rm_ftype(ftype)",
            ]
        return fw_result, bk_result

    def sync(self, connection):
        """Perform sync for the field."""
        if self.rtype.synced:
            rtype_id = self.rtype.rtype_id
        else:
            try:
                rtype_id = connection.get_rtype_id(self.rtype.bdvr_qual_name)
            except RtypeNotFound: # If the rtype doesn't exist so does field
                raise FieldNotFound

        self.field_id = connection.get_field_id(
            rtype_id, self.name
        )

    def get_migration_code(self, connection, rtype_name):
        """Return the migration code (fw, bk) for the field."""
        fw_result = []
        bk_result = []
        try:
            self.sync(connection)
        except FieldNotFound:
            fw_result.append(
                "connection.add_field(RTYPES['{0}'], FTYPES['{1}'], '{2}')".\
                    format(
                        rtype_name,
                        self.get_ftype_bdvr_qual_name(),
                        self.name,
                    )
            )
            bk_result += [
                "field = connection.get_field_id(RTYPES['{0}'], '{1}')".format(
                    rtype_name,
                    self.name,
                ),
                "connection.rm_field(field)",
            ]
        return fw_result, bk_result
