from bdvr.core.alias import Alias
from bdvr.core.field import Field

class _BaseString(Field):
    """Base class for all values represented simply as a string."""

    @property
    def table_name(self):
        return 'values_' + self.datatype

    def dehydrate(self, resource, values):
        values.setdefault(self.datatype, {})[
            self.field_id
        ] = resource.__dict__[self.name]

    def hydrate(self, resource, values):
        resource.__dict__[self.name] = values[
            self.datatype
        ].get(self.field_id, None)


class String(_BaseString):
    """String type."""

    datatype = 'character_varying'


class Text(_BaseString):
    """Long text type."""

    datatype = 'text'
