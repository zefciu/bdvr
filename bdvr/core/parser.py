"""Definition of expression parser."""
import string
import sys

import pyparsing as pp

from bdvr.core.expression.path import Path
from bdvr.core.expression._abc import Expression
from bdvr.core.expression import comparison
from bdvr.core.expression import logical
from bdvr.core.expression import literal
from bdvr.core.expression.resource import ResourceReference
from bdvr.core.expression.constant import CONST_DICT


class BdvrSyntaxError(BaseException):
    """The expression could not be parsed."""


class Parser:
    """The expression parser. A singleton."""

    expression_tokens = []

    infixes = {
        '==': comparison.Eq,
        '!=': comparison.Neq,
        '|': logical.Or,
        '&': logical.And,
    }

    prefixes = {
        '!': logical.Not,
    }


    def parse(self, inp):
        if isinstance(inp, Expression):
            return inp
        return t_expression.parseString(inp, parseAll=True)[0]

    def parse_infix(self, tocs):
        first, op, second = tocs[0]
        return self.infixes[op](first, second)

    def parse_prefix(self, tocs):
        op, expr = tocs[0]
        return self.prefixes[op](expr)


parser = Parser()
def parse_expression(s):
    """Parse an expression string, returning expression object."""
    try:
        return parser.parse(s)
    except BaseException:
        print(
            "Error when trying to parser expression {}".format(s),
            file=sys.stderr
        )
        raise

ex = parse_expression

t_identifier = pp.Word(
     string.ascii_letters, string.ascii_letters + string.digits + '_'
)

def parse_int(tocs):
    return int(tocs[0])

t_int = pp.Word(string.digits)
# t_int.setParseAction(lambda toc: int(toc))
t_int.setParseAction(parse_int)

t_single_quote = pp.Literal("'")
t_double_quote = pp.Literal('"')

t_string_single_q = t_single_quote + pp.CharsNotIn("'") + t_single_quote
t_string_double_q = t_double_quote + pp.CharsNotIn('"') + t_double_quote

t_string = t_string_single_q | t_string_double_q
t_string.setParseAction(lambda tocs: literal.String(tocs[1]))

t_literal = t_string

t_backtick = pp.Literal('`')
t_colon = pp.Literal(':')

def parse_resource_id(tocs):
    return ResourceReference(id=tocs[1])

t_resource_id = t_backtick + t_int + t_backtick
t_resource_id.setParseAction(parse_resource_id)

def parse_resource_rtype_slug(tocs):
    return ResourceReference(rtype=tocs[1], slug=tocs[3])

t_resource_rtype_slug = (
    t_backtick + t_identifier + t_colon + t_identifier + t_backtick
)
t_resource_rtype_slug.setParseAction(parse_resource_rtype_slug)

# def parse_resource_slug(tocs):
#     return ResourceReference(slug=tocs[1])
# 
# t_resource_slug = (t_backtick + t_identifier + t_backtick)
# t_resource_slug.setParseAction(parse_resource_slug)

t_resource = t_resource_id | t_resource_rtype_slug # | t_resource_slug

t_path_segment = pp.Literal('.') + t_identifier
t_path_segment.setParseAction(lambda s, loc, tocs: tocs[1])

t_path = t_path_segment * (1, None)
t_path.setParseAction(lambda s, loc, tocs: Path(tocs))

t_const = '$' + t_identifier
t_const.setParseAction(lambda tocs: CONST_DICT[tocs[1]])

t_atom = t_path | t_literal | t_const | t_resource

t_eq_op = pp.Literal('==')
t_neq_op = pp.Literal('!=')

t_comp_infix = (t_eq_op | t_neq_op)

t_or_op = pp.Literal('|')
t_and_op = pp.Literal('&')

t_logic_infix = (t_or_op | t_and_op)

t_not_prefix = pp.Literal('!')

t_logic_prefix = t_not_prefix

t_expression = pp.operatorPrecedence(t_atom, [
    (t_comp_infix, 2, pp.opAssoc.LEFT, parser.parse_infix),
    (t_not_prefix, 1, pp.opAssoc.RIGHT, parser.parse_prefix),
    (t_logic_infix, 2, pp.opAssoc.LEFT, parser.parse_infix),
])
