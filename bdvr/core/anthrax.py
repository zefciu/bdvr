"""Anthrax bindings."""

from collections import OrderedDict

from anthrax.reflector import Reflector
from anthrax.util import bind_fields
from anthrax import field as af
from anthrax import widget as aw
import pyramid.i18n

from bdvr.core.resource import Resource
from bdvr.core.field.string import String, Text
from bdvr.core.field.number import Integer
from bdvr.core.field.misc import Boolean, Action
from bdvr.core.field.relation import ManyToMany

translate = pyramid.i18n.TranslationStringFactory('bdvr')

class BdvrReflector(Reflector):
    """The object that reflects BDVR rtypes and resources as anthrax forms."""

    def get_fields(self, form):
        result = OrderedDict()
        explicit_actions = False
        if isinstance(self.source, Resource):
            source = type(self.source)
        else:
            source = self.source
        for field_name, field in source.fields.items():
            field = self._handle_field(field_name, field, source)
            if field is not None:
                result[field_name] = field
        if not explicit_actions:
            result['__save__'] = af.HttpSubmit(label=translate('Save'))

        bind_fields(result, form)
        return result

    def _handle_field(self, field_name, field, source):
        dispatcher = self._map.get(type(field))
        if dispatcher is None:
            return None
        return getattr(self, dispatcher)(field_name, field, source)

    def _string_handler(self, field_name, field, source):
        return af.TextField(label=field.label)

    def _text_handler(self, field_name, field, source):
        return af.TextField(
            label=field.label,
            widgets=[aw.LongTextInput]
        )

    def _integer_handler(self, field_name, field, source):
        return af.IntegerField(label=field.label)

    def _boolean_handler(self, field_name, field, source):
        return af.BoolField(label=field.label)

    def _m2m_handler(self, field_name, field, source):
        choices = [(r.__slug__, str(r)) for r in field.get_possible(
            source.session
        )]
        return af.MultiChoiceField(choices=choices, label=field.label)


        
    _map = {
        String: '_string_handler',
        Text: '_text_handler',
        Integer: '_integer_handler',
        Boolean: '_boolean_handler',
        ManyToMany: '_m2m_handler',
    }
