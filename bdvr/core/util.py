"""Various small utilities"""

def tuples2dict(tuples):
    """Converts an iterator of 2-tuples into a dictionary that uses the first
    value as key and the other as value. The values are stored as lists.

    >>> d = tuples2dict([('a', 1), ('b', 2), ('a', 3), ('a', 4), ('b', 5)])
    >>> d['a']
    [1, 3, 4]
    >>> d['b']
    [2, 5]
    """
    result = {}
    for key, value in tuples:
        result.setdefault(key, []).append(value)
    return result
