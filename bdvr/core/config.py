from collections.abc import Mapping, MutableMapping, Hashable
from collections import OrderedDict
import configparser

import gettext

import yaml
import pyramid.i18n

from bdvr.core import parser
from bdvr.core.security import Acl
from bdvr.core.workflow import Workflow, Transition


translate = pyramid.i18n.TranslationStringFactory('bdvr')

class ConfigLoader(yaml.Loader):
    """A modified loader that loads mappings as OrderedDicts"""

    def construct_yaml_map(self, node):
        data = OrderedDict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)

    def construct_mapping(self, node, deep=False):
        if not isinstance(node, yaml.nodes.MappingNode):
            raise yaml.error.ConstructorError(None, None,
                "expected a mapping node, but found %s" % node.id,
                node.start_mark)
        mapping = OrderedDict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            if not isinstance(key, Hashable):
                raise yaml.error.ConstructorError(
                    "while constructing a mapping", node.start_mark,
                    "found unhashable key", key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping


def trans_constructor(loader, node):
    """Constructs a translation string for a !trans tag."""
    return translate(loader.construct_scalar(node))

yaml.add_constructor(
    'tag:yaml.org,2002:map',
    ConfigLoader.construct_yaml_map,
)
yaml.add_constructor(
    'tag:yaml.org,2002:python/dict',
    ConfigLoader.construct_yaml_map,
)

yaml.add_constructor('!trans', trans_constructor)



class YamlConfig(object):
    """Generic YAML configuration file."""

    def __init__(self, file_):

        if isinstance(file_, str):
            with open(file_) as f:
                self.conf = yaml.load(f, ConfigLoader)
        else:
            self.conf = yaml.load(file_, ConfigLoader)


class Local(object):
    """The local configuration file."""

    def __init__(self, arg, *args, **kwargs):
        super(Local, self).__init__(*args, **kwargs)
        if isinstance(arg, Mapping):
            self.app_config = arg
        else:
            self.cp = configparser.ConfigParser()
            if isinstance(arg, str):
                self.cp.read(arg)
            else:
                self.cp.read_file(arg)
            self.app_config = self.cp['app:main']
        self.db_conn = self.app_config['bdvr.db_conn']
        self.module = self.app_config['bdvr.module']
        self.dojo_root = self.app_config['bdvr.dojo_root']
        self.dojo_theme = self.app_config['bdvr.dojo_theme']

class Configuration(YamlConfig, Mapping):
    """The class that represents a declarative configuration."""

    def __init__(self, file_):
        super(Configuration, self).__init__(file_)
        self.walk(self.conf)

    def __getitem__(self, key):
        return self.conf[key]

    def __len__(self):
        return len(self.conf)

    def __iter__(self):
        return iter(self.conf)

    def _match_path(self, path, pattern):
        """Performs a match for a path in a file vs a pattern with wildcards.
        """
        if len(path) != len(pattern):
            return False
        for pathpart, patternpart in zip(path, pattern):
            if patternpart == '*':
                pass
            elif isinstance(patternpart, tuple):
                if pathpart not in patternpart:
                    return False
            else:
                if pathpart != patternpart:
                    return False
        return True

    def walk(self, conf, path=None):
        """Walk down the configuration tree, parsing things if necessary."""
        if path is None:
            path = []
        else:
            path = path[:]
        for k, v in conf.items():
            new_path = path + [k]
            method = self._get_method(new_path)
            if method is not None:
                conf[k] = method(v)
                continue
            if isinstance(v, MutableMapping):
                self.walk(v, new_path)
                continue

    def _get_method(self, path):
        for pattern, method in self.PATH_CONVERTER_PAIRS:
            if self._match_path(path, pattern):
                return getattr(self, method)

    def parse_expression(self, value):
        """Parse the value as bdvr expression."""
        return parser.parse_expression(value)

    def maybe_convert_to_gettext(self, value):
        if self.conf['meta']['autotranslate']:
            return translate(value)
        else:
            return value

    def parse_acl(self, value):
        return Acl(value)

    def parse_workflow(self, value):
        transitions = []
        for transition in value:
            transitions.append(Transition(
                from_=transition['from'],
                condition = parser.parse_expression(transition['condition']),
                to_ = transition['to'],
            ))
        return Workflow(transitions)


    PATH_CONVERTER_PAIRS = [
        (['siteconfig', ('title', 'description')], 'maybe_convert_to_gettext'),
        (['rtypes', '*', 'fields', '*', 'slug_from'], 'parse_expression'),
        (['rtypes', '*', 'acl'], 'parse_acl'),
        (['rtypes', '*', 'workflow'], 'parse_workflow'),
        (['rtypes', '*', 'fields', '*', 'acl'], 'parse_acl'),
    ]
