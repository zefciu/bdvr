"""Alias creation utilities."""

from collections import namedtuple


def order_aliases(aliases):
    """:param aliases: Unordered aliases
    :return: The list of aliases ordered so the parents are always before
        children
    """
    result = []
    def append_alias(alias):
        if alias not in result:
            if alias.parent:
                append_alias(alias.parent)
            result.append(alias)
    for alias in aliases:
        append_alias(alias)
    return result

class Sequencer(object):
    """An object providing non-conflicting sequences of names.
    >>> s = Sequencer()
    >>> s('resource')
    'resource0'
    >>> s('field')
    'field0'
    >>> s('field')
    'field1'
    >>> s('resource')
    'resource1'
    >>> s('resource')
    'resource2'
    """

    def __init__(self):
        self.names = {}

    def __call__(self, base_name):
        result = base_name + str(self.names.setdefault(base_name, 0))
        self.names[base_name] += 1
        return result

Alias = namedtuple('Alias', [
    'name', 'join', 'parent', 'out_context', 'out_context_alias'
])
