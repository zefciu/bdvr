"""Functions for security."""

# We don't use pyramid security, because the authorization system of pyramid
# is related to the http layer more, while the bdvr system will have 
# more to do with resources.

from collections.abc import Sequence
import logging

from bdvr.core.parser import ex
from bdvr.core.expression.constant import TrueConst


logger = logging.getLogger(__name__)


VIEW = 'VIEW'
EDIT = 'EDIT'
ALLOW = 'ALLOW'
DENY = 'DENY'


class AclEntry(object):
    """A single entry of Access Control List."""
    
    def __init__(self, config):
        type_, user_pred, res_pred, perm = config
        if type_.lower() in {'a', 'allow'}:
            self.type_ = ALLOW
        elif type_.lower() in {'d', 'deny'}:
            self.type_ = DENY
        else:
            raise ValueError(
                'The ACL entry should be ALLOW or DENY, not {0}'.format(type_)
            )
        self.user_pred = ex(user_pred)
        self.res_pred = ex(res_pred)
        if perm.lower() in {'view,edit', 'edit,view', 've', 'ev'}:
            self.perms = {VIEW, EDIT}
        elif perm.lower() in {'view', 'v'}:
            self.perms = {VIEW}
        elif perm.lower() in {'edit', 'e'}:
            self.perms = {EDIT}
        else:
            raise ValueError(
                'The ACL perm should be VIEW and/or EDIT not {0}'.format(perm)
            )

    def __str__(self):
        result = '{} {} {} {}'.format(
            self.type_, str(self.user_pred), str(self.res_pred), self.perms
        )
        return result

    __repr__ = __str__

    def __bool__(self):
        return self.type_ == ALLOW

    def match(self, user, permission, resource=None):
        """Returns True if this entry matches the arguments."""
        try:
            user_pred_eval = self.user_pred.evaluate(user)
        except AttributeError: # Wrong actor type
            return False
        if resource is None:
            res_pred_eval = True
        else:
            res_pred_eval = self.res_pred.evaluate(resource)
        return bool(
            user_pred_eval and res_pred_eval and permission in self.perms
        )
    
            

class Acl(Sequence):
    """An Access Control List."""
    def __init__(self, config):
        self.entries = []
        for el in config:
            if isinstance(el, AclEntry):
                self.entries.append(el)
            else:
                self.entries.append(AclEntry(el))

    def __str__(self):
        return '\n'.join(str(entry) for entry in self.entries)

    __repr__ = __str__

    def match(self, user, resource, permission):
        """Returns the first entry that matches given arguments"""
        logger.info(
            'Testing {user} against {resource} for {permission}'.format(
                **locals()
            )
        )
        for entry in self:
            if entry.match(user, permission, resource):
                logger.info("Entry {} matched".format(entry))
                return entry
            logger.info("Entry {} didn't match".format(entry))

    def match_any(self, user, permission):
        for entry in self:
            if entry.match(user=user, permission=permission) and entry:
                return entry # This entry could match
            elif entry.match(
                user=user, permission=permission
            ) and entry.res_pred == TrueConst():
                return entry # This entry will match and deny

    def iter_matching(self, user, permission, resource=None, rev=False):
        """Iterates through all the matching entries."""
        it = reversed(self) if rev else self
        for entry in it:
            if entry.match(
                user=user,
                resource=resource,
                permission=permission
            ):
                yield entry

    def __len__(self):
        return len(self.entries)

    def __getitem__(self, i):
        return self.entries[i]

    def __iter__(self):
        return iter(self.entries)

    def as_expression(self, user, permission):
        """The ACL as expression specific for given user. This expression
        will be true for every resource this user has permission to and false
        for any other.
        :param user:
        :param permission:
        """
        result = None
        for entry in self.iter_matching(
            user=user, permission=permission, rev=True,
        ):
            if entry:
                if result is None:
                    result = entry.res_pred
                else:
                    result = result = result | entry.res_pred
            else:
                if result is None:
                    result = ~entry.res_pred
                else:
                    result = result = result & ~entry.res_pred
        return result



class Unauthorised(BaseException):
    """Exception raised when trying to perform unauthorised operation."""

    def __init__(self, who, perm, what):
        self.who = who
        self.perm = perm
        self.what = what

    def __str__(self):
        return "{0} is not authorised to {1} {2}".format(
            self.who, self.perm, self.what
        )
    
    __repr__ = __str__


def can(who, permission, what):
    from bdvr.core.session import is_config
    if is_config(who):
        return True
    acl = what.__acl__
    return acl.match(
        user=who,
        permission=permission,
        resource=what
    )

def can_with_any(who, permission, what):
    """The rtype-specific version of can. It returns true if there are 
    possible resources of this rtype that have this permission."""
    from bdvr.core.session import is_config
    if is_config(who):
        return True
    acl = what.__acl__
    return acl.match_any(
        user=who,
        permission=permission,
    )

EMPTY_ACL = Acl([])

class BdvrAuthorizationPolicy(object):
    """The authorization policy for pyramid mechanism"""

    def permits_rtype(self, context, principals, permission):
        return can_with_any(principals, permission, context)

    def permits(self, context, principals, permission):
        from bdvr.core.resource import Rtype
        if isinstance(context, Rtype):
            return self.permits_rtype(context, principals, permission)
        return True # TODO

    def principals_allowed_by_permission(self, context, permission):
        raise NotImplementedError() # Won't be needed
