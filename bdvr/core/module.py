import importlib
import os.path
import pkg_resources
import yaml

from bdvr.core.config import Configuration
from bdvr.core.resource import Rtype, Resource
from bdvr.core.security import EMPTY_ACL


class BdvrModule(object):
    """A module in BDVR. It is built of configuration file and optionally a
    python module containing extra objects."""

    def __init__(self, config, connection):
        self.config = Configuration(config)
        self.connection = connection
        if 'py_module' in self.config['meta']:
            self.py_module = importlib.import_module(
                self.config['meta']['py_module']
            )
        self.name = self.config['meta']['module']
        self.synced = False
        super(BdvrModule, self).__init__()

    def sync(self):
        self._sync_ftypes()
        self._sync_rtypes()
        self.synced = True

    def get_current_version(self):
        """Returns the database version of this module"""
        return self.connection.get_module_version(self.name)

    def get_rtype(self, rtype_name):
        if '.' not in rtype_name:
            rtype_name = '.'.join([self.name, rtype_name])
        return self.rtypes[rtype_name]


    def make_migration(self):
        """Return the python code that will perform model migrations for this
        module."""
        result = []
        result.append('RTYPES = {}')
        result.append('FTYPES = {}')
        result.append('')
        fw_result = []
        bk_result = []
        current_version = self.get_current_version()
        result.append('')
        self.ftypes = {}
        self.rtypes = {}
        for ftype_name, ftype_clsname in self.config.get('ftypes', {}).items():
            ftype = self._make_ftype(ftype_name, ftype_clsname)
            self.ftypes[ftype.get_ftype_bdvr_qual_name()] = ftype
            fw_code, bk_code = ftype.get_ftype_migration_code(self.connection)
            fw_result += fw_code
            bk_result += bk_code
        for rtype_name, rtype_config in self.config['rtypes'].items():
            qual_name = '.'.join([self.name, rtype_name])
            rtype = self._make_rtype(rtype_name, rtype_config)
            self.rtypes[rtype.bdvr_qual_name] = rtype
            fw_code, bk_code = rtype.get_migration_code(self.connection)
            fw_result += fw_code
            bk_result += bk_code
            for field in rtype.own_fields.values():
                fw_code, bk_code = field.get_migration_code(
                    self.connection, qual_name
                )
                fw_result += fw_code
                bk_result += bk_code

        fw_result.extend(
            [
                "connection.set_module_version('{0}', {1})".format(
                    self.name, current_version + 1
                ), 
                'connection.commit()'
            ]
        )
        bk_result.extend(
            [
                "connection.set_module_version('{0}', {1})".format(
                    self.name, current_version
                ), 
                'connection.commit()'
            ]
        )

        def indented(code):
            for line in code:
                yield ' ' * 4 + line

        result.append('def forward(connection):')
        result += indented(fw_result)
        result.append('')
        result.append('def backward(connection):')
        result += indented(bk_result)
        return current_version + 1, result

    def _sync_ftypes(self):
        """Synchronises the ftypes with database. Returns the dict of ftypes.
        """
        self.ftypes = {}
        for ftype_name, ftype_clsname in self.config['ftypes'].items():
            ftype = self._make_ftype(ftype_name, ftype_clsname) 
            self.ftypes[ftype.get_ftype_bdvr_qual_name()] = ftype

    def _sync_rtypes(self):
        """Synchronises the rtypes with database. Returns the dict of rtypes.
        """
        self.rtypes = {}
        for rtype_name, rtype_config in self.config['rtypes'].items():
            rtype = self._make_rtype(rtype_name, rtype_config) 
            rtype.sync(self.connection)
            self.rtypes[rtype.bdvr_qual_name] = rtype
        for rtype in self.rtypes.values():
            rtype.setup_relations(self)

    def _make_ftype(self, ftype_name, ftype_clsname):
        """Creates the ftype from given class path"""
        modulename, clsname = ftype_clsname.split(':')
        module = importlib.import_module(modulename)
        ftype = getattr(module, clsname)
        ftype.module = self
        return ftype

    def _make_field(self, fname, field_config):
        """Creates the field from given configuration."""
        field_config = field_config.copy()
        typename = field_config['type']
        if '.' not in typename:
            typename = '.'.join([self.name, typename])
        field_config['module'] = self
        field_config['name'] = fname
        Ftype = self.ftypes[typename]
        result = Ftype(**field_config)
        result.__acl__ = field_config.get('acl')
        return result

    def _make_rtype(self, rtype_name, rtype_config):
        """Creates the rtype from given configuration."""
        if rtype_config.get('base', False):
            bases = (Resource,)
        elif 'inherit' in rtype_config:
            bases = tuple(
                (self.rtypes[name] for name in rtype_config['inherit'])
            )
        else:
            bases = (self.get_rtype('core.base_resource'),)
        dict_ = {
            'module': self
        }
        fields_created = []
        default_field_config = rtype_config['fields'].get('__default__', {})
        for fname, field_config in rtype_config['fields'].items():
            if fname == '__default__':
                continue
            field_config_merged = default_field_config.copy()
            field_config_merged.update(field_config)
            field = self._make_field(fname, field_config_merged)
            dict_[fname] = field
            fields_created.append(field)
            dict_['__acl__'] = rtype_config.get('acl', EMPTY_ACL)
            dict_['__workflow__'] = rtype_config.get('workflow', [])
            dict_['__label__'] = rtype_config.get('label', [])
        result = Rtype(rtype_name, bases, dict_)
        for field in fields_created:
            field.rtype = result
        if 'py_class' in rtype_config:
            mod_name, class_name = rtype_config['py_class'].split(':')
            mod = importlib.import_module(mod_name)
            class_ = getattr(mod, class_name)
            result = Rtype(rtype_name, (class_, result), {})

        return result


MODULE_MAP = None


def _fill_module_map():
    """Fills the MODULE_MAP global."""
    global MODULE_MAP
    MODULE_MAP = {}
    for dist in pkg_resources.working_set:
        try:
            modules = yaml.load(dist.get_metadata('bdvr_modules.yaml'))
        except OSError:
            continue
        if not modules:
            continue
        for mod_name, path, filename in modules:
            MODULE_MAP[mod_name] = (path, filename)


def _get_resource_data(name):
    """Returns the data needed by pkg_resources to find a module."""
    if MODULE_MAP is None:
        _fill_module_map()
    try:
        return MODULE_MAP[name]
    except KeyError:
        raise ValueError('Module {0} not installed'.format(name))


def load_module(name, connection):
    """Load module with given name"""
    path, filename = _get_resource_data(name)
    conf = pkg_resources.resource_stream(path, filename)
    return BdvrModule(conf, connection)


def find_module(name):
    """Find the module with given name"""
    path, filename = _get_resource_data(name)
    return pkg_resources.resource_filename(path, filename)
