"""Workflow definitions."""

from collections.abc import Iterable

class Transition(object):
    """A transition is a single possible change of state."""

    def __init__(self, from_, condition, to_, *args, **kwargs):
        self.from_ = from_
        self.condition = condition
        self.to_ = to_
        super(Transition, self).__init__(*args, **kwargs)

    def match(self, resource):
        """Return true if resource matches this transition."""
        return (
            resource.__state__ == self.from_ and
            self.condition.evaluate(resource)
        )

    def perform(self, resource):
        """Perform the transition on a resource. NOTE: this method doesn't
        check if it *should* perform. The Workflow object does it."""
        resource.__state__ = self.to_

class Workflow(Iterable):
    """The complete workflow of an object."""

    def __init__(self, transitions):
        self._transitions = transitions

    def __len__(self):
        return len(self._transitions)

    def __iter__(self):
        return iter(self._transitions)

    def match(self, resource):
        for transition in self:
            if transition.match(resource):
                return transition

    def perform(self, resource):
        """Check if the transition should occur and perform it if necessary.
        :param resource
        :return: The tuple (before, after) of the states.
        """
        before = resource.__state__
        transition = self.match(resource)
        if transition is not None:
            transition.perform(resource)
        return before, resource.__state__
