"""Pyramid views."""

import anthrax.container
from pyramid.httpexceptions import HTTPFound


def new_form(Rtype, request):
    """The form to create a resource *ex nihilo*"""

    class Form(anthrax.container.Form):
        __reflect__ = ('bdvr', Rtype)
        __frontend__ = 'dojo'
    form = Form()
    form.kwargs['method'] = 'POST'
    if request.POST:
        form.__raw__ = request.POST
        kwargs = dict(form)
        kwargs.pop('__save__', None)
        if form.__valid__:
            resource = Rtype(by=request.user, **form)
            return HTTPFound(location='/')
    return {'form': form.render()}
