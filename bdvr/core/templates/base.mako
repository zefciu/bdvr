<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        @import url(${dojo_root}/dijit/themes/${dojo_theme}/${dojo_theme}.css);
    </style>
    <script type="text/javascript" src="${dojo_root}/dojo/dojo.js"
            data-dojo-config="isDebug: true,parseOnLoad: true" ></script>
</head>
<body class="${dojo_theme}">
    ${self.body()}
</body>
</html>
