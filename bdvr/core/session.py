"""Definitions for high-level database operations."""
import indexed_list as il

from bdvr.core import db
from bdvr.core.search import _Search
from bdvr.core.security import can, EDIT, VIEW, Unauthorised
from bdvr.core.resource import READ, WRITE, dehydrate, hydrate


class ResourceNotFound(BaseException):
    """Resource was not found (session layer)."""

class AuthenticationFailed(BaseException):
    """Raised when authentication of user fails"""

def dirty(resource):
    """Returns True if the resource is dirty (contains unsaved data)."""
    return resource.__manipulator__.dirty

def ghost(resource):
    """Returns True if the resource is a ghost
    (has not been created in DB yet)."""
    return resource.__manipulator__.ghost

def _mark_dirty(resource, is_dirty):
    resource.__manipulator__.dirty = is_dirty

def _mark_ghost(resource, is_ghost):
    resource.__manipulator__.ghost = is_ghost


class ResourcesTable(il.Table):
    resource_id = il.UniqueColumn()
    rtype_id = il.MultiColumn()
    slug = il.MultiColumn()
    resource = il.Unindexed()
    rtype_slug = il.UniqueAttributeIndex('rtype_id', 'slug')


class _ConfigResource(object):
    """An object that pretends to be a resource of a specific rtype.
    It is used by core to bootstrap the session. Don't use it to circumvent
    ACLs!"""

    __id__ = None

    def __init__(self):
        self.rtype = 'core.config'
        self.__slug__ = 'CONFIG'
        self.__state__ = 'CREATED'


def is_config(what):
    return isinstance(what, _ConfigResource)


class Session(object):
    """Session is a highter level wrapper around a connection. Sessions can
    only be created if the model is up-to-date."""

    def initialize(self, connection, module):
        """The common initialization logic done for normal
        __init__ and for prepare"""
        self.connection = connection
        self.module = module
        self.module.sync()
        self.rtypes_by_id = {
            rtype.rtype_id: rtype for rtype in self.module.rtypes.values()
        }
        self.resources = ResourcesTable()
        self.actor = None


    def __init__(self, connection, module):
        self.initialize(connection, module)

    @classmethod
    def prepare(cls, connection, module):
        """Prepare the CONFIG resource and all the 'ensure' resources"""
        # This can't be done during normal run due to possible dog-pile effect
        # And because of efficiency reasons
        self = cls.__new__(cls)
        self.initialize(connection, module)
        self.actor = self._get_config()
        for resource_config in module.config.get('ensure', []):
            self.ensure(resource_config)
        self.commit()
        self.actor = None

    def __setattr__(self, k, v):
        super(Session, self).__setattr__(k, v)

    def login(self, **kwargs):
        self.actor = self._get_config()
        actor = self.get_resource(**kwargs)
        self.actor = actor
        return actor

    def rollback(self):
        pass # Might be needed later

    def search_for_ids(self, Rtype, predicate):
        return self.connection.search_for_ids(Rtype, predicate)

    def commit(self):
        """Commits all the dirty data to connection."""
        for row in self.resources[:]:
            _, _, _, resource = row
            if ghost(resource):
                resource_id = self.connection.create_resource(
                    type(resource).rtype_id,
                    resource.__slug__,
                    resource.__state__,
                )
                _mark_ghost(resource, False)
                resource.__id__ = resource_id
                # Update the cache!
                self.resources.remove(row)
                self.add(resource)
        for _, _, _, resource in self.resources:
            if dirty(resource):
                values = {}
                dehydrate(resource, values)
                self.connection.set_values(resource.__id__, values)
        self.connection.commit()

    def ensure(self, config):
        """Process one ensure statement"""
        rtype = self.get_rtype(config['type'])
        try:
            self.connection.get_resource(
                rtype_id=rtype.rtype_id,
                slug = config['slug']
            )
            return
        except db.ResourceNotFound:
            resource = rtype(**config['data']) 

    def _get_config(self):
        """Returns the CONFIG resource for this session."""
        return _ConfigResource()

    def _add_resource(self, resource):
        """Adds a resource to local cache."""
        self.resources.append((
            resource.__id__,
            type(resource).rtype_id,
            resource.__slug__,
            resource
        ))

    def add(self, resource):
        """Adds a resource to a session. Should be called automatically while
        retrieving or creating resource."""
        actor = self.actor or self._get_config()
        if can(actor, EDIT, resource):
            self._add_resource(resource)
        else:
            raise Unauthorised(actor, EDIT, resource)
        _mark_dirty(resource, True)
        _mark_ghost(resource, True)

    def get_rtype(self, rtype_name):
        """Returns an rtype of a given name. This rtype is bound to this
        connection and resources created with it are added to it."""
        if '.' not in rtype_name:
            rtype_name = '.'.join([self.module.name, rtype_name])
        rtype = self.module.rtypes[rtype_name]
        rtype.session = self
        return rtype

    def get_resource(self, identification=None, **kwargs):
        """Get a resource by given identification."""
        if self.actor is None:
            raise ValueError(
                'Trying to get resource without logging in an actor'
            )
        return self._get_resource(identification, **kwargs)

    def _get_resource(
        self, identification=None, rtype_name=None, rtype_id=None,
        slug=None, id=None,
    ):
        if identification is None:
            id = None
        if isinstance(identification, int):
            id = identification
        if isinstance(identification, str) and ':' in identification:
            rtype_name, slug = identification.split(':')
        if rtype_name is not None:
            if '.' not in rtype_name:
                rtype_name = '.'.join([self.module.name, rtype_name])
            try:
                rtype_id = self.module.rtypes[rtype_name].rtype_id
            except KeyError:
                raise RtypeNotFound(rtype_name)
        try:
            id, rtype_id, slug, state = self.connection.get_resource(
                rtype_id, id, slug
            )
            if id in self.resources.resource_id:
                return self.resources.resource_id[id].resource
        except db.ResourceNotFound:
            # A ghost?
            if rtype_id and slug:
                try:
                    return self.resources.rtype_slug[(rtype_id, slug)].resource
                except KeyError:
                    raise ResourceNotFound('Cannot find resource')
            else:
                raise ResourceNotFound('Cannot find resource')
        else:
            Rtype = self.rtypes_by_id[rtype_id]
            Rtype.session = self
            values = self.connection.get_values(id)
            resource = Rtype.__new__(Rtype)
            resource.__id__ = id
            resource.__state__ = state
            hydrate(resource, values)
            self.add(resource)
            resource.__id__ = id
            _mark_dirty(resource, False)
            _mark_ghost(resource, False)
            return resource

    def search(self, Rtype, predicate):
        """Search for instances of rtype that meet the predicate.
        :param Rtype: Rtype object
        :param predicate: expression that should return ``True`` for resources
            we search for
        :return: A lazy sequence of resources."""
        return _Search(self, Rtype, predicate)
