"""Definitions for application."""
import argparse
import importlib
import math
import os.path
import re
import sys

from pyramid.config import Configurator
from pyramid.events import BeforeRender

from bdvr.core.config import Local, Configuration
from bdvr.core.db import Connection
from bdvr.core.module import load_module, find_module
from bdvr.core.view import new_form
from bdvr.core.authentication import LoginView, BdvrAuthenticationPolicy
from bdvr.core.security import BdvrAuthorizationPolicy, EDIT
from bdvr.core.session import Session

MIGRATION_RE = re.compile(r'^[a-zA-Z0-9_]+_(?P<version>[0-9]{5})\.py$')

def _get_module_from_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--local', type=open, default=None,
        help='The local file with the root module')
    parser.add_argument('-m', '--module', type=str, default=None,
        help='The root module')
    parser.add_argument('-c', '--connection', type=str, default=None,
        help='Connection string')
    args = parser.parse_args()
    if args.local:
        local = Local(args.local)
        connection = Connection(local.db_conn)
        module = load_module(local.module, connection)
    elif args.module and args.connection:
        connection = Connection(args.connection)
        module = load_module(args.module, connection)
    else:
        parser.error('You must specify --local or --module and --connection')
    return module


def _get_migration_name(module, version, as_file):
    """Return the name of the module of file for a migration"""
    result = '{0}_{1:05}'.format(module.name, version)
    if as_file:
        result += '.py'
    return result

def make_model_migration():
    """Entry point for creating migration files from console."""
    module = _get_module_from_args()
    new_version, code = module.make_migration()
    root_dir = os.path.join(
        os.path.dirname(find_module(module.name)),
        'model_migrations'
    )
    try:
        os.mkdir(root_dir)
    except OSError:
        pass # Probably directory already exists. If it is another problem,
             # it would crash in the next command
    with open(os.path.join(
        root_dir, _get_migration_name(module, new_version, as_file=True)
    ), 'w') as f:
        for line in code:
            print(line, file=f)


def apply_model_migration():
    """Entry point for running migrations."""
    module = _get_module_from_args()
    root_dir = os.path.join(
        os.path.dirname(find_module(module.name)),
        'model_migrations'
    )
    listing = os.listdir(root_dir)

    def version_from_filename(filename):
        m = MIGRATION_RE.match(filename)
        if m is None:
            return
        return int(m.group('version'))
    max_version = max((
        version_from_filename(filename)
        for filename in listing
        if version_from_filename(filename)
    ))
    towards = max_version
    sys.path.append(root_dir)
    current_version = module.get_current_version() 
    while current_version != towards:
        next_version = int(
            current_version + math.copysign(1, towards - current_version)
        )
        migration_name = _get_migration_name(
            module, next_version, as_file=False
        )
        print('Running migration: {0}'.format(migration_name))
        migration_module = importlib.import_module(migration_name)
        if current_version < towards:
            migration_module.forward(module.connection)
        else:
            migration_module.backward(module.connection)
        current_version = next_version
    Session.prepare(module.connection, module)
    

class App(object):
    """The application object."""

    def __init__(self, *args, **kwargs):
        self.local = Local(kwargs)
        self.login_view = LoginView(kwargs)
        self.pyramid_config = Configurator(settings=kwargs)
        self.pyramid_config.add_tween('pyramid_tm.tm_tween_factory',)
        self.pyramid_config.add_subscriber(
            self.add_renderer_globals, BeforeRender
        )
        self.pyramid_config.add_tween(
            'bdvr.core.stack.session_tween_factory',
            under='pyramid_tm.tm_tween_factory',
        )
        self.pyramid_config.set_authentication_policy(BdvrAuthenticationPolicy(
                secret=kwargs['bdvr.auth_secret'],
        ))
        self.pyramid_config.set_authorization_policy(BdvrAuthorizationPolicy())
        self.pyramid_config.add_route('home', '/') 
        self.pyramid_config.add_route('login', '/login/') 
        self.pyramid_config.add_route(
            'rtype',
            r'/{rtype}/',
            factory=self.rtype_factory,
        )
        self.pyramid_config.add_view(
            new_form,
            route_name='rtype',
            renderer='bdvr.core:templates/form.mako',
            permission=EDIT,
        )
        self.pyramid_config.add_view(
            self.login_view,
            route_name='login',
            renderer='bdvr.core:templates/form.mako',
        )
        self.pyramid_config.add_route(
            'resource',
            r'/{rtype}/{slug}',
            # factory='bdvr.core.resource.factory',
        )
        self.pyramid_app = self.pyramid_config.make_wsgi_app()

    def add_renderer_globals(self, event):
        event['dojo_root'] = self.local.dojo_root
        event['dojo_theme'] = self.local.dojo_theme

    def __call__(self, *args, **kwargs):
        return self.pyramid_app(*args, **kwargs)

    def rtype_factory(self, request):
        """Get an rtype byt request."""
        return request.bdvr_session.get_rtype(request.matchdict['rtype'])
