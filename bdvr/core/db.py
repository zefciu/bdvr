"""Low level database operations."""
import argparse
import math
import subprocess

import psycopg2
import pkg_resources

from bdvr.core.alias import Alias, order_aliases
from bdvr.core.config import Local
from bdvr.core.expression.path import Path
from bdvr.core.util import tuples2dict

def get_eav_version(connection):
    """Return the EAV version of the database."""
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT value FROM config WHERE name='eav_version';")
    except psycopg2.ProgrammingError as e:
        if e.pgcode == '42P01': # Relation does not exist
            connection.rollback()
            return 0
        raise
    return int(cursor.fetchall()[0][0])


class DbStateError(BaseException):
    """Exception raised when the database needs to be migrated."""

class EavOutdated(DbStateError):
    """Exception raised when the EAV schema is out of date."""

class ModelOutdated(DbStateError):
    """Exception raised when model is out of date."""

class RtypeNotFound(ModelOutdated):
    """Exception raised when rtype cannot be found."""

class FtypeNotFound(ModelOutdated):
    """Exception raised when ftype cannot be found."""

class FieldNotFound(ModelOutdated):
    """Exception raised when field cannot be found."""

class DbException(BaseException):
    """Base for exceptions that are related to database, but don't mean
    the necessity of migration"""

class ResourceNotFound(DbException):
    """The exception raised when the resource cannot be found."""

class Connection:
    """Object encapsulating psycopg2 connection. It allows low level operations
    on the resource model. It is not expected to work if the EAV schema is out
    of date."""

    eav_version = 1

    def __init__(self, connection):
        if isinstance(connection, str):
            self.connection_string = connection
            self.connection = psycopg2.connect(connection)
        else:
            self.connection = connection
            self.connection_string = connection.dsn
        self.check_eav_version()
        self.commit = self.connection.commit
        self.rollback = self.connection.rollback

    def cursor(self):
        """Return the cursor for the underlying db connection."""
        return self.connection.cursor()

    def check_eav_version(self):
        """Check if the version of eav is correct. If not - abort."""
        version = get_eav_version(self.connection)
        if version != self.eav_version:
            raise DbStateError('EAV schema needs to be migrated')

    def get_module_version(self, module):
        """Get the version of a module."""
        cursor = self.cursor()
        cursor.execute(
            'SELECT version FROM modules WHERE name = %s', (module,)
        )
        try:
            return cursor.fetchall()[0][0]
        except IndexError:
            return 0

    def set_module_version(self, module, version):
        """Set the version of a module."""
        cursor = self.cursor()
        cursor.execute(
            """DELETE FROM modules WHERE name = %(module)s;
            INSERT INTO modules VALUES(%(module)s, %(version)s);""",
            locals()
        )

    def get_rtype_id(self, name):
        """Get the id of rtype with given name."""
        cursor = self.cursor()
        cursor.execute('SELECT rtype_id FROM rtypes WHERE name = %s',
            (name,))
        try:
            return cursor.fetchall()[0][0]
        except IndexError:
            raise RtypeNotFound(name)

    def get_rtype_ancestors(self, id):
        """Return all the unique ancestors of a given rtype including itself.
        Note: this is not to be used as MRO. Let the python count MRO's
        """
        cursor = self.cursor()
        cursor.execute("""
            WITH recursive ancestors(id) AS (
                SELECT %s
                UNION
                SELECT parent_id FROM rtypes_inheritance, ancestors WHERE
                    rtypes_inheritance.child_id = ancestors.id
                ) SELECT id FROM ancestors;
        """, (id,))
        return [tup[0] for tup in cursor.fetchall()]

    def add_rtype(self, name, parents):
        """Add an rtype.
        :param name: The qualified name of rtype
        :param parents: The list of direct parents of this rtype
        :return: The id of newly created rtype
        """
        cursor = self.cursor()
        cursor.execute(
            'INSERT INTO rtypes(name) VALUES (%s) RETURNING rtype_id;',
            (name,)
        )
        rtype_id = cursor.fetchall()[0][0]
        for parent in parents:
            cursor.execute(
                """INSERT INTO rtypes_inheritance(parent_id, child_id) VALUES (%s, %s);""", (parent, rtype_id)
            )
        return rtype_id

    def rm_rtype(self, rtype_id):
        """Remove rtype.
        :param rtype_id: The id of rtype to remove
        """
        cursor = self.cursor()
        cursor.execute('DELETE FROM rtypes WHERE rtype_id = %s;', (rtype_id,))

    def add_ftype(self, name):
        """Add ftype.
        :param name: The qualified name of ftype
        :return: The id of ftype
        """
        cursor = self.cursor()
        cursor.execute(
            'INSERT INTO ftypes(name) VALUES (%s) RETURNING ftype_id;',
            (name,))
        return cursor.fetchall()[0][0]

    def get_ftype_id(self, name):
        """Get the ftype id.
        :param name: The qualified name of ftype
        :return: The id of ftype
        """
        cursor = self.cursor()
        cursor.execute('SELECT ftype_id FROM ftypes WHERE name = %s;',
            (name,))
        try:
            return cursor.fetchall()[0][0]
        except IndexError:
            raise FtypeNotFound(name)

    def rm_ftype(self, ftype_id):
        """Remove the ftype.
        :param ftype_id: The id of ftype
        """
        cursor = self.cursor()
        cursor.execute('DELETE FROM ftypes WHERE ftype_id = %s;', (ftype_id,))

    def add_field(self, rtype_id, ftype_id, name):
        """Add a field.
        :param ftype_id: The id of ftype.
        :param rtype_id: The id of rtype.
        :param name: The name of field.
        :return: The id of a field."""
        cursor = self.cursor()
        cursor.execute(
            """INSERT INTO fields(rtype_id, ftype_id, name)
            VALUES (%s, %s, %s) RETURNING field_id;""",
            (rtype_id, ftype_id, name),
        )
        return cursor.fetchall()[0][0]

    def get_field_id(self, rtype_id, name):
        """Get the ftype id.
        :param rtype_id: The id of rtype
        :param name: The name of field
        :return: The id of field
        """
        cursor = self.cursor()
        cursor.execute(
            """SELECT field_id FROM fields
            WHERE rtype_id = %s AND  name = %s;""",
            (rtype_id, name),
        )
        try:
            return cursor.fetchall()[0][0]
        except IndexError:
            raise FieldNotFound(name)

    def rm_field(self, field_id):
        """Remove the ftype.
        :param field_id: The id of field
        """
        cursor = self.cursor()
        cursor.execute('DELETE FROM fields WHERE field_id = %s;', (field_id,))

    def create_resource(self, rtype_id, slug, state):
        """Create a resource
        :param rtype: The id of rtype
        :param slug: The slug of a new resource
        :param state: The state of a new resource
        :return: The id of a resource"""
        cursor = self.cursor()
        cursor.execute(
            """INSERT INTO resources(rtype_id, slug, state)
            VALUES (%s, %s, %s) RETURNING resource_id;""",
            (rtype_id, slug, state)
        )
        return cursor.fetchall()[0][0]

    def get_resource(self, rtype_id=None, id=None, slug=None):
        """Get a resource by either id or slug.
        :param rtype_id: The id of rtype
        :param id: The id of resource
        :param slug: The slug of resource
        :return: The tuple (resource_id, rtype_id, slug, state)"""
        if not ((rtype_id and slug) or id):
            raise TypeError('Either rtype_id and slug or id is required')
        where = []
        args = []
        for argname, arg in [
            ('rtype_id', rtype_id),
            ('resource_id', id),
            ('slug', slug),
        ]:
            if arg is not None:
                where.append('{} = %s'.format(argname))
                args.append(arg)
        where = ' AND '.join(where)
        cursor = self.cursor()
        cursor.execute(
            """SELECT resource_id, rtype_id, slug, state
            FROM resources WHERE {};""".format(where),
            args
        )
        try:
            return cursor.fetchall()[0]
        except IndexError:
            raise ResourceNotFound()

    def update_resource(self, resource_id, slug=None, state=None):
        """Update the slug or state of a resource
        :param resource_id: The id of resource
        :param slug: The new slug (defaults to None - don't update
        :param state: The new state (defaults to None - don't update"""
        updates = []
        args = []
        for argname, arg in [
            ('slug', slug),
            ('state', state),
        ]:
            if arg is not None:
                updates.append('{} = %s'.format(argname))
                args.append(arg)
        updates = ','.join(updates)
        args.append(resource_id)
        cursor = self.cursor()
        cursor.execute(
            'UPDATE resources SET {} WHERE resource_id = %s;'.format(updates),
            args
        )

    def delete_resource(self, resource_id):
        """Delete the resource.
        :param resource_id: The id of resource
        """
        cursor = self.cursor()
        cursor.execute(
            'DELETE FROM resources WHERE resource_id = %s;', (resource_id,)
        )

    def set_values(self, resource_id, values):
        """Set the values on resource.
        :param resource_id: The id of this resource
        :param values: The mapping of values
        Example values:
        ({
            'character_varying': {
                1: 'Galahad',
                2: 'The Pure,
            },
            'integer': {
                3: 30,
            },
            'm21': { 
                4: [6, 7],
            },
            'm2m': {
                5: [8, 9]
            }
        })
        (1, 2, 3, 4, 5) are field_ids
        (6, 7, 8, 9) are other resources' ids
        """
        cursor = self.cursor()
        for type, fieldvalues in values.items():
            if type in {'m21', '12m', 'm2m'}:
                self.set_relation(cursor, resource_id, type, fieldvalues)
            else:
                self.set_scalar(cursor, resource_id, type, fieldvalues)

    def set_scalar(self, cursor, resource_id, type, fieldvalues):
        args = []
        for field_id, value in fieldvalues.items():
            args += [resource_id, field_id, value]
        tuples = ','.join(['(%s, %s, %s)'] * (len(args) // 3))
        cursor.execute(
            """DELETE FROM values_{}
               WHERE resource_id = %s AND field_id IN %s;""".format(type),
               (resource_id, tuple(fieldvalues.keys()))
        )
        cursor.execute(
            """INSERT INTO values_{0}(resource_id, field_id, value)
            VALUES {1};""".format(type, tuples),
            args,
        )

    def set_relation(self, cursor, resource_id, type, fieldvalues):
        args = []

        for field_id, values in fieldvalues.items():
            for value in values:
                args += [resource_id, value, field_id]
        tuples = ','.join(['(%s, %s, %s)'] * (len(args) // 3))
        cursor.execute(
            """DELETE FROM relations_{}
           WHERE first_resource_id = %s AND field_id IN %s;""".format(
               type
            ), (resource_id, tuple(fieldvalues.keys()))
        )
        if tuples:
            cursor.execute(
                """INSERT INTO relations_{0}(
                    first_resource_id, second_resource_id, field_id
                ) VALUES {1};""".format(type, tuples),
                args,
            )

    def get_values(self, resource_id):
        """Get the values for a given resource. This mapping contains also
        relations. For consistency all relations are returned as lists (even
        the 12m"""
        cursor = self.cursor()
        result = {}
        for type in ['character_varying', 'integer', 'boolean']:
            cursor.execute(
                """SELECT field_id, value FROM values_{}
                WHERE resource_id= %s""".format(type),
                (resource_id,),
            )
            result[type] = dict(cursor.fetchall())
        for type in ['m21', 'm2m', '12m']:
            cursor.execute(
                """SELECT field_id, second_resource_id FROM relations_{}
                WHERE first_resource_id = %s;""".format(type),
                (resource_id,),
            )
            result[type] = tuples2dict(cursor.fetchall())

        return result

    def search_for_ids(self, Rtype, predicate):
        aliases = self.get_aliases(Rtype, predicate.get_paths())
        aliases_vals = order_aliases(aliases.values())
        joins = [alias.join for alias in aliases_vals]
        join = '\n'.join(j[0] for j in joins)
        join_args = sum((tuple(j[1]) for j in joins), ())
        where, where_args = predicate.get_sql(aliases)
        where = 'root_res.rtype_id = %s AND ' + where
        where_args = (Rtype.rtype_id,) + where_args
        q = (
            """SELECT root_res.resource_id
               FROM resources as root_res {join} WHERE {where}""".format(
                   join=join, where=where
            ), tuple(join_args) + tuple(where_args)
        )
        cursor = self.cursor()
        cursor.execute(*q)
        return [row[0] for row in cursor.fetchall()]

    def get_aliases(self, Rtype, paths):
        """Compute the necessary aliases and return a dictionary of aliases
        for given paths.

        :param Rtype: The rtype that is the main context of alias finding
        :param paths: The iterable of paths
        """
        result = {}
        for path in paths:
            context = Rtype
            context_alias = 'root_res'
            subpath = []
            parent = None
            for segment in path:
                subpath.append(segment)
                if tuple(subpath) in Rtype.aliases:
                    alias = Rtype.aliases[tuple(subpath)].context
                    _, _, _, context, context_alias = alias
                else:
                    table_name = context.fields[segment].table_name
                    alias = (
                        self._get_alias_relation
                        if context.fields[segment].is_relation 
                        else self._get_alias_scalar
                    )(
                        context.fields[segment], parent, context, context_alias,
                        Rtype.sequencer,
                    )
                    Rtype.aliases[tuple(subpath)] = alias
                    context = alias.out_context
                    context_alias = alias.out_context_alias
                result[Path(subpath)] = alias
                parent = alias
        return result

    def _get_alias_relation(
        self, field, parent, context, context_alias, sequencer
    ):
        relation_table_alias = sequencer('relation')
        resource_table_alias = sequencer('resource')
        return Alias(
            resource_table_alias,
            (
                """
LEFT JOIN {table_name} AS {relation_table_alias}
    ON {relation_table_alias}.field_id = %s AND
    {relation_table_alias}.first_resource_id = {context_alias}.resource_id 
LEFT JOIN resources AS {resource_table_alias}
    ON {relation_table_alias}.second_resource_id =
        {resource_table_alias}.resource_id
""".format(
        table_name=field.table_name,
        relation_table_alias=relation_table_alias,
        resource_table_alias=resource_table_alias,
        context_alias=context_alias,
    ), (field.field_id,),),
            parent,
            field.ForeignRtype,
            resource_table_alias,
        )

    def _get_alias_scalar(
        self, field, parent, context, context_alias, sequencer
    ):
        table_alias = sequencer('vchar')
        return Alias(
            table_alias + '.value',
            ("""LEFT JOIN {table_name} AS {table_alias} ON (
                {table_alias}.field_id = %s AND
                {table_alias}.resource_id = {context_alias}.resource_id
            )""".format(
                table_name=field.table_name,
                table_alias=table_alias,
                context_alias=context_alias,
            ), (field.field_id,),),
            parent,
            context,
            context_alias
        )


def migrate_eav(connection_string, towards=None):
    """Migrate EAV schema.
    :param connection_string: The connection string for psycopg
    :param towards: The target version. Defaults to the current version
    """
    if towards is None:
        towards = Connection.eav_version
    connection = psycopg2.connect(connection_string)
    cur_version = get_eav_version(connection)
    while cur_version != towards:
        next_version = int(cur_version + math.copysign(
                1, towards - cur_version))
        sql_file = 'eav_migrations/{0:03d}-{1:03d}.sql'.format(
            cur_version, next_version)
        print ('Running: {}'.format(sql_file))
        with pkg_resources.resource_stream('bdvr.core', sql_file) as f:
            commands = f.read()
        connection.cursor().execute(commands)
        connection.commit()
        cur_version = next_version


def migrate():
    """Entry point for migrations"""
    parser = argparse.ArgumentParser('Migrate database')
    parser.add_argument('-t', dest='to_', metavar='TO', type=int, default=None,
            help='The target version. By default - the version currently '
            'supported by Bedevere.')
    parser.add_argument('-l', '--local', help='local YAML file')
    args = parser.parse_args()
    local_conf = Local(args.local)
    migrate_eav(local_conf.db_conn, args.to_)
