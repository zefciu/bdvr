"""Search utilities."""

from collections.abc import Sequence

from bdvr.core.parser import ex

class _Search(Sequence):
    """A lazy container that encompasses search results.
    :param session: A session (with actor logged in)
    :param Rtype: the rtype or rtype name of searched resources
    :param predicate: an expression that resources should match
    """

    def __init__(self, session, Rtype, predicate, *args, **kwargs):
        self.Rtype = Rtype
        self.predicate = ex(predicate)
        self.session = session
        self.ids = None # Not loaded yet
        super(_Search, self).__init__(*args, **kwargs)

    def __iter__(self):
        self.run()
        return (self.session.get_resource(id) for id in self.ids)

    def __getitem__(self, i):
        self.run()
        return self.session.get_resource(self.ids[i])

    def __len__(self):
        self.run()
        return len(self.ids)

    def _fetch_ids(self):
        """Load the ids of resources."""
        paths = self.predicate.get_paths()
        return self.session.search_for_ids(self.Rtype, self.predicate)

    def run(self):
        """Load the ids if they were not loaded yet. If they were, this
        method has no effect."""
        if self.ids is None:
            self.ids = self._fetch_ids()
