"""Abstract base class for expression."""
import abc

class Expression(metaclass=abc.ABCMeta):
    """Expression is a class that has the possibility to be evaluated against
    an existing resource (Python representation) a form containing the resource
    (JavaScript representation) and to be used in SQL."""

    @abc.abstractmethod
    def get_paths(self):
        """Return a set of paths that are used in this expression. This list
        should have the same length and order as the list of aliases  that this
        expression expects for `.get_sql()` method."""

    @abc.abstractmethod
    def evaluate(self, resource):
        """Evaluate this expression against a specified loaded resource and
        return the result."""

    @abc.abstractmethod
    def get_js(self, ctx):
        """Given the list of variable names which reflects the list returned
        by `.get_paths()` return a JavaScript expression that is an equivalent
        of this"""


    @abc.abstractmethod
    def get_sql(self, aliases):
        """Given the dict of field aliases which reflects the list returned
        by `.get_paths()` return an SQL expression that is an equivalent
        of this"""


class SimpleInfixExpression(Expression):
    """An abstract expression that:
        * Has infix notation
        * Has two arguments
        * Both arguments are evaluated in the same context.
        """

    @abc.abstractproperty
    def operator(self):
        """The operator for this expression as str."""

    @abc.abstractproperty
    def js_operator(self):
        """The operator for js representation."""

    def __init__(self, first, second):
        self.first = first
        self.second = second

    def get_js(self, ctx):
        return '{0} {1} {2}'.format(
            self.first.get_js(ctx), self.js_operator, self.second.get_js(ctx)
        )

    @abc.abstractmethod
    def python_fun(self):
        """The evaluation function that receives the two evaluated values."""

    def evaluate(self, ctx):
        return self.python_fun(
            self.first.evaluate(ctx), self.second.evaluate(ctx)
        )

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return (self.first, self.second) == (other.first, other.second)

    def __str__(self):
        return '({0}) {1} ({2})'.format(self.first, self.operator, self.second)

    __repr__ = __str__

    def get_paths(self):
        return self.first.get_paths().union(self.second.get_paths())
