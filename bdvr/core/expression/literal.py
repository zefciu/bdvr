"""Literal expressions."""

from bdvr.core.expression._abc import Expression
import json

class BaseLiteral(Expression):
    """Base literal expression. An abstract class."""

    def __init__(self, value):
        self.value = value

    def get_paths(self):
        return []

    def evaluate(self, resource):
        return self.value

    def get_js(self, ctx):
        return json.dumps(self.value)

    def __eq__(self, other):
        return self.value == other.value


class String(BaseLiteral):
    """Literal string value."""

    def __str__(self):
        return "'{0}'".format(self.value)

    def get_sql(self, aliases):
        return '%s', (self.value,)
