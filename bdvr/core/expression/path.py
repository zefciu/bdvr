"""Definition for path expression."""
from collections.abc import Sequence

import pyparsing as pp

from bdvr.core.expression._abc import Expression

class Path(Expression, Sequence):
    """The path is an expression that addresses a specific field in it's
    context."""

    def __init__(self, segments):
        self._segments = tuple(segments)

    def __str__(self):
        return ''.join(('.' + segment for segment in self._segments))

    def __hash__(self):
        return hash(self._segments)

    def __getitem__(self, i):
        return self._segments[i]

    def __iter__(self):
        return iter(self._segments)

    __repr__ = __str__

    def __eq__(self, other):
        return self._segments == other._segments

    def __len__(self):
        return len(self._segments)

    def get_paths(self):
        return {self}

    def evaluate(self, resource):
        current_resource = resource
        for segment in self._segments:
            current_resource = getattr(current_resource, segment)
        return current_resource

    def get_js(self, ctx):
        segs = ','.join("'{0}'".format(s) for s in self._segments)
        return 'bdvr.getPath({0}, [{1}])'.format(ctx, segs)

    def get_sql(self, aliases):
        name, _, _, _, _ = aliases[self]
        return (name, ())

    @property
    def subpaths(self):
        """Iterator over the subpaths in this path.
        >>> p = Path(['a', 'b', 'c'])
        >>> list(p.subpaths)
        ['.a', '.a.b', '.a.b.c']
        """
        current = []
        for segment in self._segments:
            current.append(segment)
            yield Path(current)
