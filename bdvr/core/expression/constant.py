"""Constant expressions are the ones that are independent of context. They may
sometimes not be 'constant' in the sens, that they don't change during the
course of application."""

import abc

from bdvr.core.expression._abc import Expression


class SimpleConstant(Expression):
    """A simple constant always returning the same value"""

    @abc.abstractproperty
    def python_value(self):
        """The value in python."""

    @abc.abstractproperty
    def js_value(self):
        """The value string in JavaScript."""

    def get_paths(self):
        return {}

    def evaluate(self, resource):
        return self.python_value

    def get_js(self):
        return self.js_value

    def get_sql(self, aliases):
        return ('%s', (self.python_value,))

    def __str__(self):
        return '${}'.format(self.bdvr_repr)

    __repr__ = __str__


class _TrueConst(SimpleConstant):
    """Logical true."""

    python_value = True
    js_value = 'true'


class _FalseConst(SimpleConstant):
    """Logical false."""

    python_value = False
    js_value = 'false'

class _NoneConst(SimpleConstant):
    """None / null / NULL"""
    python_value = None
    js_value = 'null'


TrueConst = _TrueConst()
FalseConst = _FalseConst()
NoneConst = _NoneConst()


CONST_DICT = {
    'True': TrueConst,
    'False': FalseConst,
    'None': NoneConst,
}


for k, v in CONST_DICT.items():
    v.bdvr_repr = k
