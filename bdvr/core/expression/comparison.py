"""Definitions for equality and comparison expressions."""
from bdvr.core.expression._abc import SimpleInfixExpression
from bdvr.core.expression.constant import NoneConst

class EqualityMixin(SimpleInfixExpression):
    """The logic that handles None as SQL NULL."""

    def get_sql(self, aliases):
        first_sql, first_args = self.first.get_sql(aliases)
        if self.second is NoneConst:
            return (
                self.sql_for_none.format(self.first.get_sql(aliases)),
                first_args
            )
        else:
            second_sql, second_args = self.second.get_sql(aliases)
            return (
                "({}) {} ({})".format(
                    first_sql, self.sql_operator, second_sql
                ),
                first_args + second_args,
            )


class Eq(EqualityMixin, SimpleInfixExpression):
    """Check for equality."""

    operator = '=='
    js_operator = '==='
    sql_operator = '='
    sql_for_none = '({}) IS NULL'

    def python_fun(self, x, y):
        return x == y


class Neq(EqualityMixin, SimpleInfixExpression):
    """Check for non-equality."""

    operator = '!='
    js_operator = '!=='
    sql_operator = '<>'
    sql_for_none = '({}) IS NOT NULL'

    def python_fun(self, x, y):
        return x != y
