"""Resource related expressions."""
from bdvr.core.expression._abc import Expression

class ResourceReference(Expression):
    """The reference to any resource. It can load the resource, but should
    mostly be used just to compare e.g. slugs."""

    def __init__(self, id=None, rtype=None, slug=None):
        self.id = id
        self.rtype = rtype
        self.slug = slug

    def __str__(self):
        if self.id is not None:
            return '`{}`'.format(self.id)
        elif self.rtype and self.slug:
            return '`{}:{}`'.format(self.rtype, self.slug)
        # else:
        #     return '`{}`'.format(self.slug)

    def __eq__(self, other):
        try:
            if self.id:
                return self.id == other.__id__
            elif self.rtype and self.slug:
                return self.rtype, self.slug == type(other), other.__slug__
            else:
                return self.slug == other.__slug__
        except AttributeError:
            return False

    def get_js(self):
        return self.id

    def get_sql(self, aliases):
        return '%s', (self.id)

    def get_paths(self):
        return set()

    def evaluate(self, ctx):
        return self
        # if self.id:
        #     return ctx.__session__.get_resource(self.id)
        # elif self.rtype and self.slug:
        #     return ctx.__session__.get_resource(
        #         rtype_name=self.rtype,
        #         slug=self.slug
        #     )
