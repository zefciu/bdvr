"""Definitions for boolean algebra expressions."""
from bdvr.core.expression._abc import Expression, SimpleInfixExpression

class Or(SimpleInfixExpression):
    """Perform logical alternative on subexpressions."""

    operator = '|'
    js_operator = '||'

    def python_fun(self, x, y):
        return x or y

    def get_sql(self, aliases):
        raise NotImplementedError


def or_expr(self, other):
    return Or(self, other)


Expression.__or__ = or_expr


class And(SimpleInfixExpression):
    """Perform logical coniunction on subexpressions."""

    operator = '&'
    js_operator = '&&'

    def python_fun(self, x, y):
        return x and y

    def get_sql(self, aliases):
        raise NotImplementedError


def and_expr(self, other):
    return And(self, other)


Expression.__and__ = and_expr


class Not(Expression):
    """Logical negation on the subexpression."""

    def __init__(self, subexpression):
        self.subexpression = subexpression

    def __str__(self):
        return ('!({})'.format(self.subexpression))

    __repr__ = __str__

    def get_js(self, ctx):
        return '!({})'.format(self.subexpression.get_js(ctx))

    def evaluate(self, ctx):
        return not self.subexpression.evaluate(ctx)

    def __eq__(self, other):
        return type(other) == Not and other.subexpression == self.subexpression

    def get_paths(self):
        return self.subexpression.get_paths()

    def get_sql(self, aliases):
        raise NotImplementedError()


def not_expr(self):
    return Not(self)


Expression.__invert__ = not_expr
