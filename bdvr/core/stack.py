"""The application stack."""

import psycopg2.pool

import transaction

from bdvr.core.config import Local
from bdvr.core.module import load_module
from bdvr.core.db import Connection
from bdvr.core.session import Session

class Manager(object):
    """The manager for tm2."""
    def __init__(self, request, pool):
        self.request = request
        self.pool = pool

    def tpc_begin(self, transaction):
        pass

    def commit(self, transaction):
        pass

    def tpc_vote(self, transaction):
        pass

    def tpc_finish(self, transaction):
        self.request.bdvr_session.commit()
        self.pool.putconn(self.request.pg_connection)

    def abort(self, transaction):
        self.request.bdvr_session.rollback()
        self.pool.putconn(self.request.pg_connection)

    def tpc_abort(self, transaction):
        self.request.bdvr_session.rollback()
        self.pool.putconn(self.request.pg_connection)

def session_tween_factory(handler, registry):
    """Create a pyramid tween that sets bdvr_connection and bdvr_session
    on the request."""
    local_config = Local(registry.settings)
    connection_pool = psycopg2.pool.ThreadedConnectionPool(
        5, 10, local_config.db_conn
    )
    module_name = local_config.module
    def session_tween(request):
        request.pg_connection = connection_pool.getconn()
        request.bdvr_connection = Connection(request.pg_connection)
        module = load_module(module_name, request.bdvr_connection)
        request.bdvr_session = Session(request.bdvr_connection, module)
        t = transaction.get()
        manager = Manager(request, connection_pool)
        t.join(manager)
        return handler(request)
    return session_tween
