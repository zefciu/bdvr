from setuptools import setup, find_packages

with open('README.rst') as f:
    long_description = f.read()

with open('entry_points.ini') as f:
    entry_points = f.read()

setup(
    name = 'Bedevere',
    version = '0.0.1',
    description = 'The wise resource management',
    long_description = long_description,
    namespace_packages = ['bdvr'],
    setup_requires = ['bdvr-setup'],
    install_requires = [
        'pyramid',
        'pyparsing',
        'psycopg2',
        'Anthrax>=0.1.0',
        'indexed_list>=0.2.0',
#        'AnhtraxDojoFrontend>=0.1.0'
    ],
    packages = find_packages(),
    entry_points = entry_points,
    bdvr_modules = [('core', 'bdvr.core', 'core.yaml')],
    include_package_data = True,
    zip_safe = True,
)
